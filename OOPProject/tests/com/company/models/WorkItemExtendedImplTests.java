package com.company.models;

import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;
import com.company.models.contracts.WorkItemExtended;
import com.company.models.enums.BugSevirity;
import com.company.models.enums.BugStatus;
import com.company.models.enums.Priority;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class WorkItemExtendedImplTests {
    private String name;
    private Member member;
    private Board board;
    private WorkItemExtended<BugStatus> workItemExtended;
    private Bug bug;

    @Before
    public void init() {
        //Arrange
        name = "nameForTest";
        member = new MemberImpl(name);
        board = new BoardImpl(name);
        workItemExtended = new WorkItemExtendedImpl(board, name, name, BugStatus.ACTIVE, Priority.HIGH, member);

        bug = new BugImpl(board, "Invalid Input",
                "Arguments should be 10.", BugStatus.ACTIVE, Priority.HIGH, member,
                BugSevirity.MAJOR, "First, should I see if argument's type is correct.");
}

    @Test
    public void getMember_Should_ReturnMember() {
        workItemExtended.getMember();
        Assert.assertEquals(member, workItemExtended.getMember());
    }

    @Test
    public void assignMember_should_AssignMemberToWorkItem_when_passedValidArguments() {
        // Act
        bug.assignMember("nameForTest");

        // Assert
        Assert.assertEquals("nameForTest", bug.getAssigneeMember());
    }
}
