package com.company.models;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.*;
import com.company.models.enums.BugSevirity;
import com.company.models.enums.BugStatus;
import com.company.models.enums.FeedbackStatus;
import com.company.models.enums.Priority;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class MemberImplTests {
    private WIMRepository wimRepository;

    private List<WorkItem<Status>> workItems;
    private String name;
    private Member member;
    private Board board;
    private Bug bug;

    @Before
    public void init() {
        //Arrange
        wimRepository = new WIMRepositoryImpl();

        name = "nameForTest";
        member = new MemberImpl(name);
        wimRepository.addMember(member.getName(), member);

        board = new BoardImpl(name);
        workItems = new ArrayList<>();
    }

    @Test
    public void workItems_Should_ReturnWorkItems() {
        member.workItems();
        Assert.assertEquals(new ArrayList<>(workItems), workItems);
    }

    @Test
    public void addWorkItem_Should_AddWorkItem() {
        //Arrange
        WorkItem workItem = new FeedbackImpl(board, name, name, FeedbackStatus.SCHEDULED,4);

        //Act
        member.addWorkItem(workItem);
        List resultWorkItem = new ArrayList<>();
        resultWorkItem.add(workItem);
        //Assert
        Assert.assertEquals(resultWorkItem, member.workItems());
    }

    @Test
    public void toString_should_printMembers_when_passedValidArguments() {
        // Arrange
        board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        bug = new BugImpl(board, "Engine commands done!",
                "Having problems with listing items.", BugStatus.ACTIVE, Priority.HIGH, member,
                BugSevirity.MAJOR, "First, should I see if argument's type is correct.");
        wimRepository.addBug(bug.getTitle(), bug);

        // Act
        member.addWorkItem(bug);
        String result = member.toString();
        String expected =  "********************" + System.lineSeparator()
                + String.format("Member's name: %s", member.getName())
                + System.lineSeparator() + "----- Work Items ----- "
                + System.lineSeparator() + bug.toString()
                + System.lineSeparator() + member.showActivityHistory()
                + "********************";

        // Assert
        Assert.assertEquals(expected, result);
    }

    @Test
    public void toString_should_printMembers_when_thereAreNoWorkItems() {
        // Act
        String result = member.toString();
        String expected =  "********************" + System.lineSeparator()
                + String.format("Member's name: %s", member.getName())
                + System.lineSeparator() + "----- No WORK ITEMS -----"
                + System.lineSeparator() + member.showActivityHistory()
                + "********************";

        // Assert
        Assert.assertEquals(expected, result);
    }
}
