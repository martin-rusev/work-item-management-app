package com.company.models;

import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;
import com.company.models.enums.BugSevirity;
import com.company.models.enums.BugStatus;
import com.company.models.enums.Priority;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BoardImplTests {
    private Member member;
    private Board board;
    private Bug bug;

    @Before
    public void before() {
        board = new BoardImpl("What to do?");

        member = new MemberImpl("Stanimir");

        bug = new BugImpl(board, "Invalid Input",
                "Arguments should be 10.", BugStatus.ACTIVE, Priority.HIGH, member,
                BugSevirity.MAJOR, "First, should I see if argument's type is correct.");
    }

    @Test
    public void toString_should_printBoard_when_notAddActivityAndWorkItems() {
        // Act
        String result = board.toString();
        String expected = "********************" + System.lineSeparator()
                + String.format("Board's name: %s", board.getName())
                + System.lineSeparator() + "----- No work items -----"
                + System.lineSeparator() + board.showActivityHistory()
                + "********************" + System.lineSeparator();

        // Assert
        Assert.assertEquals(expected, result);
    }

    @Test
    public void toString_should_printBoard_when_passedValidArguments() {
        // Act
        board.addWorkItem(bug);
        board.addActivity("Board created successfully");

        String result = board.toString();
        String expected = "********************" + System.lineSeparator()
                + String.format("Board's name: %s", board.getName())
                + System.lineSeparator() + "----- Work Items ----- "
                + System.lineSeparator() + bug.toString() + System.lineSeparator()
                + board.showActivityHistory() + "********************"
                + System.lineSeparator();

        // Assert
        Assert.assertEquals(expected, result);
    }
}
