package com.company.models;

import com.company.models.contracts.ActivityHistory;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TeamImplTests {

    private List<Member> members;
    private List<Board> boards;
    private ActivityHistory activityHistory;
    private List<String> activityHistoryRepo;
    private String name;
    private String activity;
    private Member member;
    private Team team;
    private Board board;

    @Before
    public void init() {
        //Arrange
        members = new ArrayList<>();
        boards = new ArrayList<>();
        activityHistory = new ActivityHistoryImpl();
        activityHistoryRepo = new ArrayList<>();
        name = "nameForTest";
        activity = "activity";
        member = new MemberImpl(name);
        team = new TeamImpl(name);
        board = new BoardImpl(name);
    }

    @Test
    public void addMember_Should_AddMember() {
        //Act
        team.addMember(member);
        ArrayList resultMembers = new ArrayList<>();
        resultMembers.add(member);
        //Assert
        Assert.assertEquals(resultMembers, team.getMembers());
    }

    @Test
    public void addBoard_Should_AddBoard() {
        //Act
        team.addBoard(board);
        ArrayList resultBoards = new ArrayList<>();
        resultBoards.add(board);
        //Assert
        Assert.assertEquals(resultBoards, team.getBoards());
    }

    @Test
    public void getMembers_Should_ReturnMembers() {
        team.getMembers();
        Assert.assertEquals(new ArrayList<>(members), members);
    }

    @Test
    public void getBoards_Should_ReturnBoards() {
        List<Board> boards = team.getBoards();
        Assert.assertEquals(new ArrayList<>(boards), new ArrayList<>());
    }

    @Test
    public void getName_Should_ReturnName() {
        Assert.assertEquals(team.getName(), name);
    }

    @Test
    public void printMembers_Should_ReturnNoMembers() {
        String noMembers = String.format("%s", "----- No team members -----") +
                System.lineSeparator();
        Assert.assertEquals(team.printMembers(), noMembers);
    }

    @Test
    public void printMembers_Should_ReturnMembers() {
        team.addMember(member);
        String members = String.format("%s", "----- Members ----- ") +
                System.lineSeparator() +
                member.getName() +
                System.lineSeparator();
        Assert.assertEquals(team.printMembers(), members);
    }
    @Test
    public void printBoards_Should_ReturnNoBoards() {
        String noBoards = String.format("%s", "----- No boards  -----") +
                System.lineSeparator();
        Assert.assertEquals(team.printBoards(), noBoards);
    }
    @Test
    public void printBoards_Should_ReturnBoards() {
        team.addBoard(board);
        String boards = String.format("%s", "----- Boards ----- ") +
                System.lineSeparator() +
                board.getName() +
                System.lineSeparator();
        Assert.assertEquals(team.printBoards(), boards);
    }
    @Test
    public void toString_Should_ReturnConcatenatedValues() {
        String SEPARATOR = "********************";
        String TEAM_TO_STRING = "Team's name: %s";
        team.addBoard(board);
        team.addMember(member);
        String expected = SEPARATOR + System.lineSeparator() + String.format(TEAM_TO_STRING, name)
                + System.lineSeparator() + team.printMembers() + team.printBoards()
                + team.showActivityHistory() + SEPARATOR + System.lineSeparator();

        Assert.assertEquals(team.toString() ,expected);
    }

}

