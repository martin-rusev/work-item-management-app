package com.company.commands.change;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.BoardImpl;
import com.company.models.MemberImpl;
import com.company.models.StoryImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Story;
import com.company.models.enums.Priority;
import com.company.models.enums.StorySize;
import com.company.models.enums.StoryStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeStoryAttributesTests {
    private WIMRepository wimRepository;
    private Command testCommand;

    private Member member;
    private Board board;
    private Story story;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ChangeStoryAttributes(wimRepository);

        member =  new MemberImpl("Boris");
        wimRepository.addMember("Boris", member);

        board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        story = new StoryImpl(board, "Hello World", "Refactor code",
                StoryStatus.INPROGRESS, Priority.LOW, member, StorySize.SMALL);
        wimRepository.addStory(story.getTitle(), story);
    }

    @Test
    public void execute_should_changeStoryAttributes_when_passedValidInputAndChangedStateIsPriority() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(story.getTitle());
        arguments.add("priority");
        arguments.add("medium");

        // Act
        testCommand.execute(arguments);

        // Assert
        Assert.assertEquals(Priority.MEDIUM, story.getPriority());
    }

    @Test
    public void execute_should_changeStoryAttributes_when_passedValidInputAndChangedStateIsSize() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(story.getTitle());
        arguments.add("size");
        arguments.add("large");

        // Act
        testCommand.execute(arguments);

        // Assert
        Assert.assertEquals(StorySize.LARGE, story.getStorySize());
    }

    @Test
    public void execute_should_changeStoryAttributes_when_passedValidInputAndChangedStateIsStatus() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(story.getTitle());
        arguments.add("status");
        arguments.add("doNe");

        // Act
        testCommand.execute(arguments);

        // Assert
        Assert.assertEquals(StoryStatus.DONE, story.getStatus());
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_passedStatusNotExist() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(story.getTitle());
        arguments.add("status");
        arguments.add("GooD");

        // Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_passedSizeNotExist() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(story.getTitle());
        arguments.add("size");
        arguments.add("BIG");

        // Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_passedPriorityNotExist() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(story.getTitle());
        arguments.add("priority");
        arguments.add("sMaLl");

        // Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_passedStateNotExist() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(story.getTitle());
        arguments.add("title");
        arguments.add("Engine command classes DONE");

        // Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_storyNotExist() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add("Hello dear");
        arguments.add("size");
        arguments.add("small");

        // Act
        testCommand.execute(arguments);
    }
}