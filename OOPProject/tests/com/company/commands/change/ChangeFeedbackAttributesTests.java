package com.company.commands.change;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.BoardImpl;
import com.company.models.FeedbackImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Feedback;
import com.company.models.enums.FeedbackStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeFeedbackAttributesTests {

    private WIMRepository wimRepository;
    private Command testCommand;

    private Board board;
    private Feedback feedback;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ChangeFeedbackAttributes(wimRepository);

        board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        feedback = new FeedbackImpl(board, "Hello World", "Refactor code",
                FeedbackStatus.SCHEDULED, 15);
        wimRepository.addFeedback(feedback.getTitle(), feedback);
    }

    @Test
    public void execute_should_changeFeedbackAttributes_when_passedValidInputAndChangedStateIsStatus() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(feedback.getTitle());
        arguments.add("status");
        arguments.add("new");

        // Act
        testCommand.execute(arguments);

        // Assert
        Assert.assertEquals(FeedbackStatus.NEW, feedback.getStatus());
    }

    @Test
    public void execute_should_changeFeedbackAttributes_when_passedValidInputAndChangedStateIsRating() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(feedback.getTitle());
        arguments.add("rating");
        arguments.add("9");

        // Act
        testCommand.execute(arguments);

        // Assert
        Assert.assertEquals(9, feedback.getRating());
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_passedStatusNotExist() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(feedback.getTitle());
        arguments.add("status");
        arguments.add("fixed");

        // Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_passedRatingIsNotValid() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(feedback.getTitle());
        arguments.add("rating");
        arguments.add("anythingElse");

        // Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_passedStateNotExist() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(feedback.getTitle());
        arguments.add("stepsToReproduce");
        arguments.add("First, I should validate methods");

        // Act
        testCommand.execute(arguments);
    }
}
