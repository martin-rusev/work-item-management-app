package com.company.commands.change;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.BoardImpl;
import com.company.models.BugImpl;
import com.company.models.MemberImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;
import com.company.models.enums.BugSevirity;
import com.company.models.enums.BugStatus;
import com.company.models.enums.Priority;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeBugAttributesTests {
    private WIMRepository wimRepository;
    private Command testCommand;

    private Member member;
    private Board board;
    private Bug bug;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ChangeBugAttributes(wimRepository);

        member =  new MemberImpl("Boris");
        wimRepository.addMember("Boris", member);

        board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        bug = new BugImpl(board, "Invalid Input",
                "Arguments should be 10.", BugStatus.ACTIVE, Priority.HIGH, member,
                BugSevirity.MAJOR, "First, should I see if argument's type is correct.");
        wimRepository.addBug(bug.getTitle(), bug);
    }

    @Test
    public void execute_should_changeBugAttributes_when_passedValidInputAndChangedStateIsPriority() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(bug.getTitle());
        arguments.add("priority");
        arguments.add("low");

        // Act
        testCommand.execute(arguments);

        // Assert
        Assert.assertEquals(Priority.LOW, bug.getPriority());
    }

    @Test
    public void execute_should_changeBugAttributes_when_passedValidInputAndChangedStateIsStatus() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(bug.getTitle());
        arguments.add("status");
        arguments.add("fixed");

        // Act
        testCommand.execute(arguments);

        // Assert
        Assert.assertEquals(BugStatus.FIXED, bug.getStatus());
    }

    @Test
    public void execute_should_changeBugAttributes_when_passedValidInputAndChangedStateIsSeverity() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(bug.getTitle());
        arguments.add("severity");
        arguments.add("critical");

        // Act
        testCommand.execute(arguments);

        // Assert
        Assert.assertEquals(BugSevirity.CRITICAL, bug.getBugSeverity());
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_passedSeverityNotExist() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(bug.getTitle());
        arguments.add("severity");
        arguments.add("BIG");

        // Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_passedPriorityNotExist() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(bug.getTitle());
        arguments.add("priority");
        arguments.add("So High");

        // Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_passedStatusNotExist() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(bug.getTitle());
        arguments.add("status");
        arguments.add("INPROGRESS");

        // Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_passedStateNotExist() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(bug.getTitle());
        arguments.add("activityHistory");
        arguments.add("DONE");

        // Act
        testCommand.execute(arguments);
    }
}
