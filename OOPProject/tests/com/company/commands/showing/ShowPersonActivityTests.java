package com.company.commands.showing;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.MemberImpl;
import com.company.models.contracts.Member;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowPersonActivityTests {
    private WIMRepository wimRepository;
    private Command testCommand;
    private Member member;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ShowPersonActivity(wimRepository);

        member = new MemberImpl("Stanimir");
        wimRepository.addMember("Stanimir", member);
    }

    @Test
    public void execute_should_showPersonActivity_when_passedValidWorkItems() {
        // Arrange
        member.addActivity(String
                .format("Member %s assign successfully to the bug.", member.getName()));

        List<String> arguments = new ArrayList<>();
        arguments.add(member.getName());

        // Act
        String result = testCommand.execute(arguments);
        String expected = member.showActivityHistory();

        // Assert
        Assert.assertEquals(expected, result);
    }
}
