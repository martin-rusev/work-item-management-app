package com.company.commands.showing;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.MemberImpl;
import com.company.models.TeamImpl;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowTeamMembersTests {
    private WIMRepository wimRepository;
    private Command testCommand;

    private Team team;
    private Member member_1;
    private Member member_2;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ShowTeamMembers(wimRepository);
    }

    @Test
    public void execute_should_showTeamMembers_when_passedValidInput() {
        // Arrange
        team = new TeamImpl("Coder's club");
        wimRepository.addTeam(team.getName(), team);

        member_1 = new MemberImpl("Pavleta");
        member_2 = new MemberImpl("Martin");

        team.addMember(member_1);
        team.addMember(member_2);

        List<String> arguments = new ArrayList<>();
        arguments.add(team.getName());

        // Act
        String result = testCommand.execute(arguments);
        String expected = team.printMembers();

        // Assert
        Assert.assertEquals(expected, result);
    }
}
