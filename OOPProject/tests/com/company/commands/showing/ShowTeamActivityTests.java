package com.company.commands.showing;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.TeamImpl;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowTeamActivityTests {
    private WIMRepository wimRepository;
    private Command testCommand;
    private Team team;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ShowTeamActivity(wimRepository);

        team = new TeamImpl("Cloud 7");
        wimRepository.addTeam(team.getName(), team);
    }

    @Test
    public void execute_should_showTeamActivity_when_passedValidWorkItems() {
        // Arrange
        team.addActivity(String
                .format("Performance Team %s.", team.getName()));

        List<String> arguments = new ArrayList<>();
        arguments.add(team.getName());

        // Act
        String result = testCommand.execute(arguments);
        String expected = team.showActivityHistory();

        // Assert
        Assert.assertEquals(expected, result);
    }
}
