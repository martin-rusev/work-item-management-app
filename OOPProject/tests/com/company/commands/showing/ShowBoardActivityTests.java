package com.company.commands.showing;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.BoardImpl;
import com.company.models.MemberImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowBoardActivityTests {
    private WIMRepository wimRepository;
    private Command testCommand;
    private Member member;
    private Board board;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ShowBoardActivity(wimRepository);

        member = new MemberImpl("Stanimir");
        wimRepository.addMember("Stanimir", member);

        board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);
    }

    @Test
    public void execute_should_showBoardActivity_when_passedValidWorkItems() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(board.getName());

        // Act
        String result = testCommand.execute(arguments);
        String expected = board.showActivityHistory();

        // Assert
        Assert.assertEquals(expected, result);
    }
}
