package com.company.commands.showing;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.TeamImpl;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

public class ShowTeamsTests {
    private WIMRepository wimRepository;
    private Command testCommand;

    private Team team_1;
    private Team team_2;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ShowTeams(wimRepository);
    }

    @Test
    public void execute_should_showTeams_when_passedValidInput() {
        // Arrange
        team_1 = new TeamImpl("Valeriq");
        wimRepository.addTeam(team_1.getName(), team_1);

        team_2 = new TeamImpl("Hristina");
        wimRepository.addTeam(team_2.getName(), team_2);

        // Act
        String result = testCommand.execute(Collections.emptyList());
        String expected = "All teams: " + team_1.getName()
                + ", " + team_2.getName();

        // Assert
        Assert.assertEquals(expected, result);
    }
}
