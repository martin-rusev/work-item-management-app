package com.company.commands.showing;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.BoardImpl;
import com.company.models.MemberImpl;
import com.company.models.TeamImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowBoardsTests {
    private WIMRepository wimRepository;
    private Command testCommand;

    private Member member;
    private Board board;
    private Team team;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ShowBoards(wimRepository);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_passedInvalidNumberOfArguments() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add("Member1");
        arguments.add("Member2");

        //Act
        testCommand.execute(arguments);
    }
    @Test
    public void execute_should_showBoard_when_passedValidInput() {
        // Arrange
        member = new MemberImpl("Stanimir");
        wimRepository.addMember("Stanimir", member);

        board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        team = new TeamImpl("Ivannn");
        wimRepository.addTeam(team.getName(), team);
        team.addBoard(board);

        List<String> arguments = new ArrayList<>();
        arguments.add(team.getName());

        // Act
        String result = testCommand.execute(arguments);
        String expected = team.printBoards();

        // Assert
        Assert.assertEquals(expected, result);
    }
}
