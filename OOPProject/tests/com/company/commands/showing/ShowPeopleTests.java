package com.company.commands.showing;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.MemberImpl;
import com.company.models.contracts.Member;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

public class ShowPeopleTests {

    private WIMRepository wimRepository;
    private Command testCommand;

    private Member member_1;
    private Member member_2;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ShowPeople(wimRepository);

        member_1 = new MemberImpl("Stanimir");
        wimRepository.addMember(member_1.getName(), member_1);

        member_2 = new MemberImpl("Teodora");
        wimRepository.addMember(member_2.getName(), member_2);
    }

    @Test
    public void execute_should_showPeople_when_passedValidInput() {
        // Act
        String result = testCommand.execute(Collections.emptyList());
        String expected = "All people: " + member_2.getName()
                + ", " + member_1.getName();

        // Assert
        Assert.assertEquals(expected, result);
    }
}
