package com.company.commands;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.BoardImpl;
import com.company.models.MemberImpl;
import com.company.models.StoryImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Story;
import com.company.models.enums.Priority;
import com.company.models.enums.StorySize;
import com.company.models.enums.StoryStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

public class AssignStoryToPersonTests {

    private WIMRepository wimRepository;
    private Command testCommand;

    private Member member;
    private Board board;
    private Story story_1;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new AssignStoryToPerson(wimRepository);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange, Act & Assert
        testCommand.execute(emptyList());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange, Act & Assert
        testCommand.execute(asList(new String[3]));
    }
    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_storyNotExist() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add("Hello dear");
        arguments.add("size");

        // Act
        testCommand.execute(arguments);
    }

    @Test
    public void execute_should_unAssignStoryToPerson_when_passedValidArguments() {
        // Arrange
        member = new MemberImpl("Stanimir");
        wimRepository.addMember("Stanimir", member);

        board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        story_1 = new StoryImpl(board, "Story of the WIM Project", "Description of the steps to prepare the OOP project",
                StoryStatus.INPROGRESS, Priority.HIGH, member, StorySize.LARGE);
        wimRepository.addStory(story_1.getTitle(), story_1);

        List<String> arguments = new ArrayList<>();
        arguments.add(story_1.getTitle());
        arguments.add(member.getName());

        // Act
        String result = testCommand.execute(arguments);

        String expected = String.format("Story %s successfully assign to member %s.",
                story_1.getTitle(), member.getName());

        // Assert
        Assert.assertEquals(expected, result);
    }

}

