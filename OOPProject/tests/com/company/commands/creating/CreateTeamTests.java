package com.company.commands.creating;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMFactory;
import com.company.core.contracts.WIMRepository;
import com.company.core.factories.WIMFactoryImpl;
import com.company.models.TeamImpl;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateTeamTests {
    private WIMFactory wimFactory;
    private WIMRepository wimRepository;
    private Command testCommand;

    @Before
    public void before() {
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new CreateTeam(wimFactory, wimRepository);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_passedInvalidNumberOfArguments() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add("All Stars");
        arguments.add("Coder's Club");

        //Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamAlreadyExist() {
        // Arrange
        Team team = new TeamImpl("The Best");
        wimRepository.addTeam("The Best", team);

        Team team_1 = new TeamImpl("The Best");
        wimRepository.addTeam("The Best", team_1);

        List<String> arguments = new ArrayList<>();
        arguments.add("The Best");

        // Act
        testCommand.execute(arguments);
    }


    @Test
    public void execute_should_createTeam_when_passedValidInput() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add("Doing Engine");

        // Act
        testCommand.execute(arguments);

        // Assert
        Assert.assertEquals(1, wimRepository.getTeams().size());
    }
}
