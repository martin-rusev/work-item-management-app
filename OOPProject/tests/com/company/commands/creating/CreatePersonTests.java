package com.company.commands.creating;

import com.company.commands.creating.CreatePerson;
import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMFactory;
import com.company.core.contracts.WIMRepository;
import com.company.core.factories.WIMFactoryImpl;
import com.company.models.MemberImpl;
import com.company.models.contracts.Member;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreatePersonTests {
    private WIMFactory wimFactory;
    private WIMRepository wimRepository;
    private Command testCommand;

    @Before
    public void before() {
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new CreatePerson(wimFactory, wimRepository);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_passedInvalidNumberOfArguments() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add("Member1");
        arguments.add("Member2");

        //Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberAlreadyExist() {
        // Arrange
        Member member = new MemberImpl("Pavletka");
        wimRepository.addMember("Pavletka", member);

        List<String> arguments = new ArrayList<>();
        arguments.add("Pavletka");

        // Act
        testCommand.execute(arguments);
    }

    @Test
    public void execute_should_createPerson_when_passedValidInput() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add("Pavletka");

        // Act
        testCommand.execute(arguments);

        // Assert
        Assert.assertEquals(1, wimRepository.getMembers().size());
    }

}
