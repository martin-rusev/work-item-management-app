package com.company.commands.creating;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMFactory;
import com.company.core.contracts.WIMRepository;
import com.company.core.factories.WIMFactoryImpl;
import com.company.models.BoardImpl;
import com.company.models.BugImpl;
import com.company.models.MemberImpl;
import com.company.models.TeamImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import com.company.models.enums.BugSevirity;
import com.company.models.enums.BugStatus;
import com.company.models.enums.Priority;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateBugTests {
    private WIMFactory wimFactory;
    private WIMRepository wimRepository;
    private Command testCommand;

    @Before
    public void before() {
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new CreateBug(wimFactory, wimRepository);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_passedInvalidNumberOfArguments() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add("Team Cookies");
        arguments.add("Critical");

        //Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_boardNotExist() {
        // Arrange
        Member member = new MemberImpl("Stanimir");
        wimRepository.addMember("Stanimir", member);

        Team team = new TeamImpl("Big Stars");
        wimRepository.addTeam("Big Stars", team);

        List<String> arguments = new ArrayList<>();
        arguments.add("What to do now?");
        arguments.add("Invalid Input.");
        arguments.add("Arguments should be 10.");
        arguments.add("active");
        arguments.add("high");
        arguments.add("Stanimir");
        arguments.add("major");
        arguments.add("First, should I see if argument's type is correct.");

        // Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberNotExist() {
        // Arrange
        Board board = new BoardImpl("What to do?");
        wimRepository.addBoard("What to do?", board);

        Team team = new TeamImpl("Big Stars");
        wimRepository.addTeam("Big Stars", team);

        List<String> arguments = new ArrayList<>();
        arguments.add("What to do?");
        arguments.add("Invalid Input.");
        arguments.add("Arguments should be 10.");
        arguments.add("active");
        arguments.add("high");
        arguments.add("Stani");
        arguments.add("major");
        arguments.add("First, should I see if argument's type is correct.");

        // Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_bugAlreadyExist() {
        // Arrange
        Member member = new MemberImpl("Stanimir");
        wimRepository.addMember("Stanimir", member);

        Board board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        Bug bug = new BugImpl(board, "Invalid Input.",
                "Arguments should be 10.", BugStatus.ACTIVE, Priority.HIGH, member,
                BugSevirity.MAJOR, "First, should I see if argument's type is correct.");
        wimRepository.addBug(bug.getTitle(), bug);

        List<String> arguments = new ArrayList<>();
        arguments.add("What to do?");
        arguments.add("Invalid Input.");
        arguments.add("Arguments should be 10.");
        arguments.add("active");
        arguments.add("high");
        arguments.add("Stanimir");
        arguments.add("major");
        arguments.add("First, should I see if argument's type is correct.");

        // Act
        testCommand.execute(arguments);
    }

    @Test
    public void execute_should_createBug_when_passedValidInput() {
        // Arrange
        Member member = new MemberImpl("Stanimir");
        wimRepository.addMember("Stanimir", member);

        Team team = new TeamImpl("Big Stars");
        wimRepository.addTeam("Big Stars", team);

        Board board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        List<String> arguments = new ArrayList<>();
        arguments.add(board.getName());
        arguments.add("Invalid Input.");
        arguments.add("Arguments should be 10.");
        arguments.add("active");
        arguments.add("high");
        arguments.add("Stanimir");
        arguments.add("major");
        arguments.add("First, should I see if argument's type is correct.");

        // Act
        testCommand.execute(arguments);

        // Assert
        Assert.assertEquals(1, wimRepository.getBugs().size());
    }
}
