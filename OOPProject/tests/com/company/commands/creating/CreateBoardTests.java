package com.company.commands.creating;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMFactory;
import com.company.core.contracts.WIMRepository;
import com.company.core.factories.WIMFactoryImpl;
import com.company.models.BoardImpl;
import com.company.models.TeamImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateBoardTests {
    private WIMFactory wimFactory;
    private WIMRepository wimRepository;
    private Command testCommand;

    @Before
    public void before() {
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new CreateBoard(wimFactory, wimRepository);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_passedInvalidNumberOfArguments() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add("5");
        arguments.add("6");
        arguments.add("7");

        //Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_passedInvalidIndex() {
        // Arrange
        List<String> arguments = new ArrayList<>();

        Team team = new TeamImpl("Ivan Ivanov");

        arguments.add(team.getName());
        arguments.add("Things ToDo");

        wimRepository.addBoard("Hristo Vasilev", null);
        // Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamNotExist() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add("Stars");
        arguments.add("Pesho");

        // Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_boardAlreadyExist() {
        // Arrange
        Board board = new BoardImpl("Stars");
        wimRepository.addBoard("Stars", board);

        Team team = new TeamImpl("Ivana");
        wimRepository.addTeam("Ivana", team);

        List<String> arguments = new ArrayList<>();
        arguments.add(team.getName());
        arguments.add("Stars");

        // Act
        testCommand.execute(arguments);
    }

    @Test
    public void execute_should_createBoard_when_passedValidInput() {
        // Arrange
        Team team = new TeamImpl("Ivana");
        wimRepository.addTeam("Ivana", team);

        List<String> arguments = new ArrayList<>();
        arguments.add(team.getName());
        arguments.add("Pesho");

        // Act
        testCommand.execute(arguments);

        // Assert
        Assert.assertEquals(1, wimRepository.getBoards().size());
    }
}
