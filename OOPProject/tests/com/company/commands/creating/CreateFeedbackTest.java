package com.company.commands.creating;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMFactory;
import com.company.core.contracts.WIMRepository;
import com.company.core.factories.WIMFactoryImpl;
import com.company.models.BoardImpl;
import com.company.models.FeedbackImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Feedback;
import com.company.models.enums.FeedbackStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateFeedbackTest {
    private WIMFactory wimFactory;
    private WIMRepository wimRepository;
    private Command testCommand;

    @Before
    public void before() {
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new CreateFeedback(wimFactory, wimRepository);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_passedInvalidNumberOfArguments() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add("Team Cookies");
        arguments.add("Critical");

        //Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_feedbackAlreadyExist() {
        // Arrange
        Board board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        Feedback feedback = new FeedbackImpl(board, "Hello World", "Refactor code",
                FeedbackStatus.SCHEDULED, 15);
        wimRepository.addFeedback(feedback.getTitle(), feedback);

        List<String> arguments = new ArrayList<>();
        arguments.add(board.getName());
        arguments.add("Hello World");
        arguments.add("Refactor code");
        arguments.add("scheduled");
        arguments.add("15");

        // Act
        testCommand.execute(arguments);
    }

    @Test
    public void execute_should_createFeedback_when_passedValidInput() {
        // Arrange
        Board board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        List<String> arguments = new ArrayList<>();
        arguments.add(board.getName());
        arguments.add("Hello World");
        arguments.add("Refactor code");
        arguments.add("scheduled");
        arguments.add("15");

        // Act
        testCommand.execute(arguments);

        // Assert
        Assert.assertEquals(1, wimRepository.getFeedbacks().size());
    }
}
