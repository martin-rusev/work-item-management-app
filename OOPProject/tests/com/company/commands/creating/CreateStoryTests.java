package com.company.commands.creating;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMFactory;
import com.company.core.contracts.WIMRepository;
import com.company.core.factories.WIMFactoryImpl;
import com.company.models.BoardImpl;
import com.company.models.FeedbackImpl;
import com.company.models.MemberImpl;
import com.company.models.StoryImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Feedback;
import com.company.models.contracts.Member;
import com.company.models.contracts.Story;
import com.company.models.enums.FeedbackStatus;
import com.company.models.enums.Priority;
import com.company.models.enums.StorySize;
import com.company.models.enums.StoryStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateStoryTests {
    private WIMFactory wimFactory;
    private WIMRepository wimRepository;
    private Command testCommand;

    @Before
    public void before() {
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new CreateStory(wimFactory, wimRepository);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_storyAlreadyExist() {
        // Arrange
        Member member = new MemberImpl("Stanimir");
        wimRepository.addMember("Stanimir", member);

        Board board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        Story story = new StoryImpl(board, "Hello World", "Refactor code",
                StoryStatus.INPROGRESS, Priority.LOW, member, StorySize.SMALL);
        wimRepository.addStory(story.getTitle(), story);

        List<String> arguments = new ArrayList<>();
        arguments.add(board.getName());
        arguments.add("Hello World");
        arguments.add("Refactor code");
        arguments.add("inprogress");
        arguments.add("low");
        arguments.add(member.getName());
        arguments.add("small");

        // Act
        testCommand.execute(arguments);
    }

    @Test
    public void execute_should_createStory_when_passedValidInput() {
        // Arrange
        Member member = new MemberImpl("Stanimir");
        wimRepository.addMember("Stanimir", member);

        Board board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        List<String> arguments = new ArrayList<>();
        arguments.add(board.getName());
        arguments.add("Hello World");
        arguments.add("Refactor code");
        arguments.add("inprogress");
        arguments.add("low");
        arguments.add(member.getName());
        arguments.add("small");

        // Act
        testCommand.execute(arguments);

        // Assert
        Assert.assertEquals(1, wimRepository.getStories().size());
    }
}
