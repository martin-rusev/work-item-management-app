package com.company.commands.adding;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMFactory;
import com.company.core.contracts.WIMRepository;
import com.company.core.factories.WIMFactoryImpl;
import com.company.models.*;
import com.company.models.contracts.*;
import com.company.models.enums.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AddCommentTests {
    private WIMFactory wimFactory;
    private WIMRepository wimRepository;
    private Command testCommand;

    @Before
    public void before() {
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new AddComment(wimFactory, wimRepository);
    }

    @Test
    public void execute_should_addComment_when_passedValidInputAndWorkItemIsBug() {
        // Arrange
        Member member =  new MemberImpl("Boris");
        wimRepository.addMember("Boris", member);

        Board board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        Bug bug = new BugImpl(board, "Invalid Input",
                "Arguments should be 10.", BugStatus.ACTIVE, Priority.HIGH, member,
                BugSevirity.MAJOR, "First, should I see if argument's type is correct.");
        wimRepository.addBug(bug.getTitle(), bug);

        List<String> arguments = new ArrayList<>();
        arguments.add("Comment description");
        arguments.add(member.getName());
        arguments.add(bug.getTitle());

        // Act
        testCommand.execute(arguments);

        // Assert
        Assert.assertEquals(1, bug.getComments().size());
    }

    @Test
    public void execute_should_addComment_when_passedValidInputAndWorkItemIsStory() {
        // Arrange
        Member member =  new MemberImpl("Boris");
        wimRepository.addMember("Boris", member);

        Board board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        Story story = new StoryImpl(board, "Invalid Input",
                "Arguments should be 10.", StoryStatus.NOTDONE,
                Priority.HIGH,  member, StorySize.LARGE);
        wimRepository.addStory(story.getTitle(), story);

        List<String> arguments = new ArrayList<>();
        arguments.add("Comment description");
        arguments.add(member.getName());
        arguments.add(story.getTitle());

        // Act
        testCommand.execute(arguments);

        // Assert
        Assert.assertEquals(1, story.getComments().size());
    }

    @Test
    public void execute_should_addComment_when_passedValidInputAndWorkItemIsFeedback() {
        // Arrange
        Member member =  new MemberImpl("Boris");
        wimRepository.addMember("Boris", member);

        Board board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        Feedback feedback = new FeedbackImpl(board, "Invalid Input",
                "Arguments should be 10.", FeedbackStatus.SCHEDULED,
                9);
        wimRepository.addFeedback(feedback.getTitle(), feedback);

        List<String> arguments = new ArrayList<>();
        arguments.add("Comment description");
        arguments.add(member.getName());
        arguments.add(feedback.getTitle());

        // Act
        testCommand.execute(arguments);

        // Assert
        Assert.assertEquals(1, feedback.getComments().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_workItemNotExist() {
        // Arrange
        Member member =  new MemberImpl("Boris");
        wimRepository.addMember("Boris", member);

        Board board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        List<String> arguments = new ArrayList<>();
        arguments.add("Comment description");
        arguments.add(member.getName());
        arguments.add("Iliya");

        // Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_memberNotExist() {
        // Arrange
        Board board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        Feedback feedback = new FeedbackImpl(board, "Invalid Input",
                "Arguments should be 10.", FeedbackStatus.SCHEDULED,
                9);
        wimRepository.addFeedback(feedback.getTitle(), feedback);

        List<String> arguments = new ArrayList<>();
        arguments.add("Comment description");
        arguments.add("Boris");
        arguments.add(feedback.getTitle());

        // Act
        testCommand.execute(arguments);
    }
}
