package com.company.commands.adding;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.MemberImpl;
import com.company.models.TeamImpl;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AddPersonToTeamTest {

    private WIMRepository wimRepository;
    private Command testCommand;
    private Member member;
    private Team team;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new AddPersonToTeam(wimRepository);

        member =  new MemberImpl("Boris");
        wimRepository.addMember(member.getName(), member);

        team = new TeamImpl("Coders' club");
        wimRepository.addTeam(team.getName(), team);
    }

    @Test
    public void execute_should_addPersonToTeam_when_passedValidInput() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add(team.getName());
        arguments.add(member.getName());

        // Act
        String expected = String.format("Person %s added successfully to team %s.",
                member.getName(), team.getName());
        String result = testCommand.execute(arguments);

        // Assert
        Assert.assertEquals(expected, result);
    }

   @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwEcxeption_when_parametersSizeIsInvalid(){
        // Arrange
        List<String> params = new ArrayList<>();
        params.add("1");
        params.add("1");
        params.add("1");
        params.add("1");

        // Act
        testCommand.execute(params);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_ShouldThrowTeamExistException(){
        // Arrange
         List<String> params = new ArrayList<>();
         params.add("Telerik");
         params.add("Martin");

         // Act
         wimRepository.addMember("Martin", member);
         testCommand.execute(params);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_ShouldThrowMemberExistException(){
        // Act
        testCommand.execute(Collections.emptyList());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberAlreadyExist() {
        // Arrange
        Member member = new MemberImpl("Pavletka");
        wimRepository.addMember("Pavletka", member);

        List<String> arguments = new ArrayList<>();
        arguments.add("Pavletka");

        // Act
        testCommand.execute(arguments);
    }
}
