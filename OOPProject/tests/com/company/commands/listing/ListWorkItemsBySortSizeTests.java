package com.company.commands.listing;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.BoardImpl;
import com.company.models.MemberImpl;
import com.company.models.StoryImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Story;
import com.company.models.enums.Priority;
import com.company.models.enums.StorySize;
import com.company.models.enums.StoryStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

public class ListWorkItemsBySortSizeTests {
    private WIMRepository wimRepository;
    private Command testCommand;
    private Member member;
    private Board board;
    private Story story_1;
    private Story story_2;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ListWorkItemsBySortSize(wimRepository);

        member = new MemberImpl("Stanimir");
        wimRepository.addMember("Stanimir", member);

        board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        story_1 = new StoryImpl(board, "Hello World", "Refactor code",
                StoryStatus.INPROGRESS, Priority.LOW, member, StorySize.SMALL);
        wimRepository.addStory(story_1.getTitle(), story_1);

        story_2 = new StoryImpl(board, "WIM Repository", "Refactor code",
                StoryStatus.NOTDONE, Priority.LOW, member, StorySize.LARGE);
        wimRepository.addStory(story_2.getTitle(), story_2);
    }

    @Test
    public void execute_should_listWorkItemsBySortSize_when_passedValidWorkItems() {
        // Arrange, Act
        String result = testCommand.execute(Collections.emptyList());
        String expected = story_2.toString()
                + story_1.toString();

        // Assert
        Assert.assertEquals(expected, result);
    }
}
