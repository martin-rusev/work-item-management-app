package com.company.commands.listing;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.BoardImpl;
import com.company.models.BugImpl;
import com.company.models.MemberImpl;
import com.company.models.StoryImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;
import com.company.models.contracts.Story;
import com.company.models.enums.*;
import org.junit.Before;

public class ListWorkItemsByFilterStatusTests {
    private WIMRepository wimRepository;
    private Command testCommand;

    private Member member;
    private Board board;
    private Story story;
    private Bug bug;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ListWorkItemsByFilterStatus(wimRepository);

        member = new MemberImpl("Stanimir");
        wimRepository.addMember("Stanimir", member);

        board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        story = new StoryImpl(board, "Hello World", "Refactor code",
                StoryStatus.INPROGRESS, Priority.LOW, member, StorySize.SMALL);
        wimRepository.addStory(story.getTitle(), story);

        bug = new BugImpl(board, "Invalid Input",
                "Arguments should be 10.", BugStatus.FIXED, Priority.HIGH, member,
                BugSevirity.MAJOR, "First, should I see if argument's type is correct.");
        wimRepository.addBug(bug.getTitle(), bug);
    }
}
