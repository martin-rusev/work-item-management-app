package com.company.commands.listing;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.*;
import com.company.models.contracts.*;
import com.company.models.enums.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

public class ListWorkItemsBySortTitleTests {
    private WIMRepository wimRepository;
    private Command testCommand;
    private Member member;
    private Board board;
    private Bug bug;
    private Story story;
    private Feedback feedback;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ListWorkItemsBySortTitle(wimRepository);

        member = new MemberImpl("Stanimir");
        wimRepository.addMember("Stanimir", member);

        board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        story = new StoryImpl(board, "Hello World", "Refactor code",
                StoryStatus.INPROGRESS, Priority.LOW, member, StorySize.SMALL);
        wimRepository.addStory(story.getTitle(), story);

        feedback = new FeedbackImpl(board, "Improve your soft skills", "Refactor code",
                FeedbackStatus.SCHEDULED, 15);
        wimRepository.addFeedback(feedback.getTitle(), feedback);

        bug = new BugImpl(board, "Invalid Input",
                "Arguments should be 10.", BugStatus.ACTIVE, Priority.HIGH, member,
                BugSevirity.MAJOR, "First, should I see if argument's type is correct.");
        wimRepository.addBug(bug.getTitle(), bug);
    }

    @Test
    public void execute_should_listWorkItemsBySortTitle_when_passedValidWorkItems() {
        // Arrange, Act
        String result = testCommand.execute(Collections.emptyList());
        String expected = story.toString()
                + feedback.toString()
                + bug.toString();

        // Assert
        Assert.assertEquals(expected, result);
    }
}
