package com.company.commands.listing;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.BoardImpl;
import com.company.models.BugImpl;
import com.company.models.CommentImpl;
import com.company.models.MemberImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Comment;
import com.company.models.contracts.Member;
import com.company.models.enums.BugSevirity;
import com.company.models.enums.BugStatus;
import com.company.models.enums.Priority;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

public class ListBugsTests {

    private WIMRepository wimRepository;
    private Command testCommand;
    private Member member;
    private Board board;
    private Bug bug_2;
    private Bug bug_1;
    private Comment comment;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ListBugs(wimRepository);

        member = new MemberImpl("Stanimir");
        wimRepository.addMember("Stanimir", member);

        board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        bug_1 = new BugImpl(board, "Engine commands done!",
                "Having problems with listing items.", BugStatus.ACTIVE, Priority.HIGH, member,
                BugSevirity.MAJOR, "First, should I see if argument's type is correct.");
        wimRepository.addBug(bug_1.getTitle(), bug_1);

        bug_2 = new BugImpl(board, "Invalid Input",
                "Arguments should be 10.", BugStatus.ACTIVE, Priority.HIGH, member,
                BugSevirity.MINOR, "First, should I see if argument's type is correct.");
        comment = new CommentImpl("Comment text", "Pavletka");
        bug_2.addComment(comment);
        bug_2.addActivity("Activity test!");
        wimRepository.addBug(bug_2.getTitle(), bug_2);
    }

    @Test
    public void execute_should_listBugs_when_passedValidWorkItems() {
        // Arrange, Act
        String result = testCommand.execute(Collections.emptyList());
        String expected = bug_2.toString()
                + bug_1.toString();

        // Assert
        Assert.assertEquals(expected, result);
    }
}
