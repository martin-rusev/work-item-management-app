package com.company.commands.listing;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.*;
import com.company.models.contracts.*;
import com.company.models.enums.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

public class ListAllWorkItemsTests {
    private WIMRepository wimRepository;
    private Command testCommand;
    private Member member;
    private Board board;
    private Bug bug;
    private Story story_1;
    private Story story_2;
    private Feedback feedback;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ListAllWorkItems(wimRepository);

        member = new MemberImpl("Stanimir");
        wimRepository.addMember("Stanimir", member);

        board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        story_1 = new StoryImpl(board, "Hello World", "Refactor code",
                StoryStatus.INPROGRESS, Priority.LOW, member, StorySize.SMALL);
        wimRepository.addStory(story_1.getTitle(), story_1);

        feedback = new FeedbackImpl(board, "Improve your soft skills", "Refactor code",
                FeedbackStatus.SCHEDULED, 15);
        wimRepository.addFeedback(feedback.getTitle(), feedback);

        bug = new BugImpl(board, "Invalid Input",
                "Arguments should be 10.", BugStatus.ACTIVE, Priority.HIGH, member,
                BugSevirity.MAJOR, "First, should I see if argument's type is correct.");
        wimRepository.addBug(bug.getTitle(), bug);

        story_2 = new StoryImpl(board, "Good luck to everyone!", "Refactor code",
                StoryStatus.INPROGRESS, Priority.LOW, member, StorySize.SMALL);
        wimRepository.addStory(story_2.getTitle(), story_2);
    }

    @Test
    public void execute_should_listAllWorkItems_when_passedValidWorkItems() {
        // Arrange, Act
        String result = testCommand.execute(Collections.emptyList());
        String expected = bug.toString()
                + story_2.toString()
                + story_1.toString()
                + feedback.toString();

        // Assert
        Assert.assertEquals(expected, result);
    }
}
