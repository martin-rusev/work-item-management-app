package com.company.commands.listing;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.*;
import com.company.models.contracts.*;
import com.company.models.enums.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

public class ListWorkItemsBySortRatingTests {
    private WIMRepository wimRepository;
    private Command testCommand;
    private Member member;
    private Board board;
    private Feedback feedback_1;
    private Feedback feedback_2;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ListWorkItemsBySortRating(wimRepository);

        member = new MemberImpl("Stanimir");
        wimRepository.addMember("Stanimir", member);

        board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        feedback_1 = new FeedbackImpl(board, "Improve your soft skills", "Refactor code",
                FeedbackStatus.SCHEDULED, 15);
        wimRepository.addFeedback(feedback_1.getTitle(), feedback_1);

        feedback_2 = new FeedbackImpl(board, "What to do now?", "Refactor code",
                FeedbackStatus.SCHEDULED, 9);
        wimRepository.addFeedback(feedback_2.getTitle(), feedback_2);
    }

    @Test
    public void execute_should_listWorkItemsBySortRating_when_passedValidWorkItems() {
        // Arrange, Act
        String result = testCommand.execute(Collections.emptyList());
        String expected = feedback_2.toString()
                + feedback_1.toString();

        // Assert
        Assert.assertEquals(expected, result);
    }
}
