package com.company.commands.listing;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.BoardImpl;
import com.company.models.FeedbackImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Feedback;
import com.company.models.enums.FeedbackStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

public class ListFeedbackTests {
    private WIMRepository wimRepository;
    private Command testCommand;
    private Board board;
    private Feedback feedback_1;
    private Feedback feedback_2;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ListFeedback(wimRepository);

        board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        feedback_1 = new FeedbackImpl(board, "Communication skills ON", "How to manage with stress?",
                FeedbackStatus.NEW, 9);
        wimRepository.addFeedback(feedback_1.getTitle(), feedback_1);

        feedback_2 = new FeedbackImpl(board, "Improve your soft skills", "Refactor code",
                FeedbackStatus.SCHEDULED, 15);
        wimRepository.addFeedback(feedback_2.getTitle(), feedback_2);
    }

    @Test
    public void execute_should_listFeedback_when_passedValidWorkItems() {
        // Arrange, Act
        String result = testCommand.execute(Collections.emptyList());
        String expected = feedback_1.toString()
                + feedback_2.toString();

        // Assert
        Assert.assertEquals(expected, result);
    }
}
