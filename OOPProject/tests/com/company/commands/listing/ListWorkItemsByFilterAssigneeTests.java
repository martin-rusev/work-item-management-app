package com.company.commands.listing;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.BoardImpl;
import com.company.models.BugImpl;
import com.company.models.MemberImpl;
import com.company.models.TeamImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import com.company.models.enums.BugSevirity;
import com.company.models.enums.BugStatus;
import com.company.models.enums.Priority;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ListWorkItemsByFilterAssigneeTests {
    private WIMRepository wimRepository;
    private Command testCommand;
    private Member member;
    private Team team;
    private Board board;
    private Bug bug;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ListWorkItemsByFilterAssignee(wimRepository);

        member = new MemberImpl("Stanimir");
        wimRepository.addMember(member.getName(), member);

        team = new TeamImpl("Breakfast club");
        wimRepository.addTeam(team.getName(), team);
    }

    @Test
    public void execute_should_printNoAssigneeMessage_when_thereIsNoAssigneeToTeam() {
        // Arrange
        board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        bug = new BugImpl(board, "Engine commands done!",
                "Having problems with listing items.", BugStatus.ACTIVE, Priority.HIGH, member,
                BugSevirity.MAJOR, "First, should I see if argument's type is correct.");
        wimRepository.addBug(bug.getTitle(), bug);

        List<String> arguments = new ArrayList<>();
        arguments.add(member.getName());

        // Act
        String result = testCommand.execute(arguments);
        String expected = "There are no assignees to the team.";

        // Assert
        Assert.assertEquals(expected, result);
    }
}
