package com.company.commands;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.BoardImpl;
import com.company.models.BugImpl;
import com.company.models.MemberImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;
import com.company.models.enums.BugSevirity;
import com.company.models.enums.BugStatus;
import com.company.models.enums.Priority;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

public class AssignBugToPersonTests {

    private WIMRepository wimRepository;
    private Command testCommand;

    private Member member;
    private Board board;
    private Bug bug;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new AssignBugToPerson(wimRepository);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange, Act & Assert
        testCommand.execute(emptyList());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange, Act & Assert
        testCommand.execute(asList(new String[3]));
    }
    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_storyNotExist() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add("Hello dear");
        arguments.add("size");

        // Act
        testCommand.execute(arguments);
    }

    @Test
    public void execute_should_assignBugToPerson_when_passedValidArguments() {
        // Arrange
        member = new MemberImpl("Stanimir");
        wimRepository.addMember("Stanimir", member);

        board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        bug = new BugImpl(board, "Engine commands done!",
                "Having problems with listing items.", BugStatus.ACTIVE, Priority.HIGH, member,
                BugSevirity.MAJOR, "First, should I see if argument's type is correct.");
        wimRepository.addBug(bug.getTitle(), bug);

        List<String> arguments = new ArrayList<>();
        arguments.add(bug.getTitle());
        arguments.add(member.getName());

        // Act
        String result = testCommand.execute(arguments);

        String expected = String.format("Bug %s successfully assign to member %s.",
                bug.getTitle(), member.getName());

        // Assert
        Assert.assertEquals(expected, result);
    }
}









