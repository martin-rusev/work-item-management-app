package com.company.commands;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.BoardImpl;
import com.company.models.BugImpl;
import com.company.models.MemberImpl;
import com.company.models.TeamImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import com.company.models.enums.BugSevirity;
import com.company.models.enums.BugStatus;
import com.company.models.enums.Priority;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class UnAssignBugToPersonTests {

    private WIMRepository wimRepository;
    private Command testCommand;
    private Member member;
    private Board board;
    private Bug bug;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new UnAssignBugToPerson(wimRepository);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_passedInvalidNumberOfArguments() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add("Team Cookies");
        arguments.add("Critical");

        //Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberNotExist() {
        // Arrange
        Board board = new BoardImpl("What to do?");
        wimRepository.addBoard("What to do?", board);

        Team team = new TeamImpl("Big Stars");
        wimRepository.addTeam("Big Stars", team);

        List<String> arguments = new ArrayList<>();
        arguments.add("What to do?");
        arguments.add("Invalid Input.");

        // Act
        testCommand.execute(arguments);
    }

    @Test
    public void execute_should_unAssugBugToPerson_when_passedValidArguments() {
        // Arrange
        member = new MemberImpl("Stanimir");
        wimRepository.addMember("Stanimir", member);

        board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        bug = new BugImpl(board, "Engine commands done!",
                "Having problems with listing items.", BugStatus.ACTIVE, Priority.HIGH, member,
                BugSevirity.MAJOR, "First, should I see if argument's type is correct.");
        wimRepository.addBug(bug.getTitle(), bug);

        List<String> arguments = new ArrayList<>();
        arguments.add(bug.getTitle());
        arguments.add(member.getName());

        // Act
        String result = testCommand.execute(arguments);

        String expected = String.format("Bug %s successfully unassign from member %s.",
                bug.getTitle(), member.getName());

        // Assert
        Assert.assertEquals(expected, result);
    }
}
