package com.company.commands;

import com.company.core.WIMRepositoryImpl;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.BoardImpl;
import com.company.models.MemberImpl;
import com.company.models.StoryImpl;
import com.company.models.TeamImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Story;
import com.company.models.contracts.Team;
import com.company.models.enums.Priority;
import com.company.models.enums.StorySize;
import com.company.models.enums.StoryStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class UnAssignStoryToPersonTests {
    private WIMRepository wimRepository;
    private Command testCommand;
    private Member member;
    private Board board;
    private Story story_1;

    @Before
    public void before() {
        wimRepository = new WIMRepositoryImpl();
        testCommand = new UnAssignStoryToPerson(wimRepository);
    }

    @Test(expected = IllegalArgumentException.class) // Assert
    public void execute_should_throwException_when_passedInvalidNumberOfArguments() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add("Team Cookies");
        arguments.add("Critical");

        //Act
        testCommand.execute(arguments);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberNotExist() {
        // Arrange
        Board board = new BoardImpl("What to do?");
        wimRepository.addBoard("What to do?", board);

        Team team = new TeamImpl("Big Stars");
        wimRepository.addTeam("Big Stars", team);

        List<String> arguments = new ArrayList<>();
        arguments.add("What to do?");
        arguments.add("Invalid Input.");

        // Act
        testCommand.execute(arguments);
    }

    @Test
    public void execute_should_unAssignStoryToPerson_when_passedValidArguments() {
        // Arrange
        member = new MemberImpl("Stanimir");
        wimRepository.addMember("Stanimir", member);

        board = new BoardImpl("What to do?");
        wimRepository.addBoard(board.getName(), board);

        story_1 = new StoryImpl(board, "Story of the WIM Project", "Description of the steps to prepare the OOP project",
                StoryStatus.INPROGRESS, Priority.HIGH, member, StorySize.LARGE);
        wimRepository.addStory(story_1.getTitle(), story_1);

        List<String> arguments = new ArrayList<>();
        arguments.add(story_1.getTitle());
        arguments.add(member.getName());

        // Act
        String result = testCommand.execute(arguments);

        String expected = String.format("Story %s successfully unassign from member %s.",
                story_1.getTitle(), member.getName());

        // Assert
        Assert.assertEquals(expected, result);
    }
}
