package com.company.models.contracts;

import java.util.List;

public interface Board {
    String getName();

    List<WorkItem> workItems();

    void addActivity(String activity);

    void addWorkItem(WorkItem workItem);

    String showActivityHistory();
}
