package com.company.models.contracts;

import com.company.models.enums.StorySize;

public interface Story extends WorkItemExtended {
    StorySize getStorySize();

    void setStorySize(StorySize storySize);
}
