package com.company.models.contracts;

import com.company.models.enums.BugSevirity;

import java.util.List;

public interface Bug extends WorkItemExtended {

    String getStepsToReproduce();

    BugSevirity getBugSeverity();

    void setSeverity(BugSevirity severity);
}
