package com.company.models.contracts;

import java.util.List;

public interface Team {

    String getName();

    List<Member> getMembers();

    List<Board> getBoards();

    void addBoard(Board board);

    void addMember(Member member);

    void addActivity(String activity);

    String showActivityHistory();

    String printMembers();

    String printBoards();


}
