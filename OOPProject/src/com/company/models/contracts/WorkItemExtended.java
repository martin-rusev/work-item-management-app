package com.company.models.contracts;

import com.company.models.enums.Priority;

public interface WorkItemExtended<T extends Status> extends WorkItem {
    Priority getPriority();

    Member getMember();

    void setPriority(Priority priority);

    void assignMember(String person);

    void unAssignMember(String person);

    String getAssigneeMember();
}
