package com.company.models.contracts;

import java.util.List;

public interface WorkItem<T extends Status> extends Commentable {

    Board getBoard();

    String getTitle();

    String getDescription();

    T getStatus();

    List<Comment> getComments();

    void setStatus(T status);

    void addActivity(String activity);

    void addComment(Comment comment);
}
