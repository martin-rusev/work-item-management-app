package com.company.models.contracts;

import java.util.List;

public interface Member {

    String getName();

    List<WorkItem> workItems();

    void addWorkItem(WorkItem workItem);

    void addActivity(String activity);

    String showActivityHistory();
}
