package com.company.models.contracts;

import java.util.List;

public interface ActivityHistory {
    List<String> getActivityHistory();

    void addActivityHistory(String activity);
}
