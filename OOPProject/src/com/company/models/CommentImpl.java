package com.company.models;

import com.company.models.contracts.Comment;
import com.company.models.validators.ValidatorHelper;

public class CommentImpl implements Comment {
    private static final String CONTENT_IS_EMPTY_ERROR_MESSAGE = "Content cannot be empty.";
    private static final String AUTHOR_IS_EMPTY_ERROR_MESSAGE = "Author cannot be empty.";

    private String content;
    private String author;

    public CommentImpl(String content, String author) {
        setContent(content);
        setAuthor(author);
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        return String.format("%s comments \"%s\"",
                getAuthor(), getContent())
                + System.lineSeparator();
    }

    private void setContent(String content) {
        ValidatorHelper.validateEmptyString(content, CONTENT_IS_EMPTY_ERROR_MESSAGE);
        this.content = content;
    }

    private void setAuthor(String author) {
        ValidatorHelper.validateEmptyString(author, AUTHOR_IS_EMPTY_ERROR_MESSAGE);
        this.author = author;
    }
}
