package com.company.models;

import com.company.models.contracts.ActivityHistory;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import com.company.models.validators.ValidatorHelper;

import java.util.ArrayList;
import java.util.List;

public class TeamImpl implements Team {

    private static final int NAME_MIN_LENGTH = 5;
    private static final int NAME_MAX_LENGTH = 15;
    private static final String NAME_LENGTH_ERROR_MASSAGE_FORMAT =
            String.format("The Name's length cannot be less than %d or more than %d symbols long.",
            NAME_MIN_LENGTH, NAME_MAX_LENGTH);
    private static final String ACTIVITY_IS_EMPTY_ERROR_MESSAGE = "Activity cannot be empty.";
    private static final String SEPARATOR = "********************";
    private static final String NO_ACTIVITY_MESSAGE = "----- No activity history -----";
    private static final String TEAM_ACTIVITY_SEPARATOR = "----- Team's activity history -----";
    private static final String NO_MEMBERS = "----- No team members -----";
    private static final String MEMBERS_HEADER = "----- Members ----- ";
    private static final String NAME_IS_EMPTY_ERROR_MESSAGE = "Name cannot be empty.";
    private static final String NO_BOARDS = "----- No boards  -----";
    private static final String BOARDS_HEADER = "----- Boards ----- ";
    private final static String TEAM_TO_STRING = "Team's name: %s";

    private String name;
    private List<Member> members;
    private List<Board> boards;
    private ActivityHistory activityHistory;

    public TeamImpl(String name) {
        setName(name);
        members = new ArrayList<>();
        boards = new ArrayList<>();
        activityHistory = new ActivityHistoryImpl();
    }

    public List<Member> getMembers() {
        return new ArrayList<>(members);
    }

    public List<Board> getBoards() {
        return new ArrayList<>(boards);
    }

    public String getName() {
        return name;
    }

    public void addBoard(Board board) {
        boards.add(board);
    }

    @Override
    public void addMember(Member member) {
        members.add(member);
    }

    @Override
    public void addActivity(String activity) {
        activityHistory.addActivityHistory(activity);
    }

    @Override
    public String showActivityHistory() {
        StringBuilder activity = new StringBuilder();

        if(activityHistory.getActivityHistory().isEmpty()) {
            activity.append(NO_ACTIVITY_MESSAGE).append(System.lineSeparator());
        } else {
            activity.append(TEAM_ACTIVITY_SEPARATOR)
                    .append(System.lineSeparator());

            for (String myString : activityHistory.getActivityHistory()) {
                activity.append(myString).append(System.lineSeparator());
            }
        }

        return activity.toString();
    }

    @Override
    public String printMembers() {
        StringBuilder stringBuilder = new StringBuilder();

        if (members.isEmpty()) {
            stringBuilder.append(String.format("%s", NO_MEMBERS))
                    .append(System.lineSeparator());
        } else {
            stringBuilder.append(String.format("%s", MEMBERS_HEADER)).
                    append(System.lineSeparator());

            for (Member member : members) {
                stringBuilder.append(member.getName())
                        .append(System.lineSeparator());
            }
        }
        return stringBuilder.toString();
    }

    @Override
    public String printBoards() {
        StringBuilder stringBuilder = new StringBuilder();

        if (boards.isEmpty()) {
            stringBuilder.append(String.format("%s", NO_BOARDS))
                    .append(System.lineSeparator());
        } else {
            stringBuilder.append(String.format("%s", BOARDS_HEADER))
                    .append(System.lineSeparator());

            for (Board board : boards) {
                stringBuilder.append(board.getName())
                        .append(System.lineSeparator());
            }
        }
        return stringBuilder.toString();
    }

    @Override
    public String toString() {
        return SEPARATOR + System.lineSeparator() + String.format(TEAM_TO_STRING, getName())
                + System.lineSeparator() + printMembers() + printBoards()
                + showActivityHistory() + SEPARATOR + System.lineSeparator();
    }

    private void setName(String name) {
        ValidatorHelper.validateIntRange(name.length(), NAME_MIN_LENGTH,
                NAME_MAX_LENGTH, NAME_LENGTH_ERROR_MASSAGE_FORMAT);
        ValidatorHelper.validateEmptyString(name, NAME_IS_EMPTY_ERROR_MESSAGE);
        this.name = name;
    }
}
