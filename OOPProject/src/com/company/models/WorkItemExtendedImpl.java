package com.company.models;

import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Status;
import com.company.models.contracts.WorkItemExtended;
import com.company.models.enums.Priority;

public class WorkItemExtendedImpl<T extends Status> extends WorkItemImpl implements WorkItemExtended {
    private static final String PRIORITY_FIELD = "Priority";
    private static final String ASSIGNEE_FIELD = "Assignee";

    private Priority priority;
    private Member member;
    private String assigneeMember;

    public WorkItemExtendedImpl(Board board, String title, String description, Status status, Priority priority, Member member) {
        super(board, title, description, status);
        setPriority(priority);
        setMember(member);
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    @Override
    public Member getMember() {
        return member;
    }

    public String getAssigneeMember() {
        return member.getName();
    }

    @Override
    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    @Override
    public void assignMember(String person) {
        setAssigneeMember(person);
    }

    @Override
    public void unAssignMember(String person) {
        setAssigneeMember("");
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("%s: %s" + System.lineSeparator() + "%s: %s" + System.lineSeparator(),
                PRIORITY_FIELD, getPriority(),
                ASSIGNEE_FIELD, getMember().getName());
    }

    private void setMember(Member member) {
        this.member = member;
    }

    private void setAssigneeMember(String assigneeMember) {
        this.assigneeMember = assigneeMember;
    }
}
