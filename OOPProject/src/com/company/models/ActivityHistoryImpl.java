package com.company.models;

import com.company.models.contracts.ActivityHistory;
import com.company.models.validators.ValidatorHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ActivityHistoryImpl implements ActivityHistory {

    private static final String DATE_PATTERN = "MM/dd/yyy HH:mm:ss";
    private static final String DATE_MESSAGE_FORMAT = "%s: %s";
    private static final String ACTIVITY_IS_EMPTY_ERROR_MESSAGE = "Activity cannot be empty.";

    private List<String> activityHistory;

    public ActivityHistoryImpl() {
        activityHistory = new ArrayList<>();
    }

    public List<String> getActivityHistory() {
        return new ArrayList<>(activityHistory);
    }

    public void addActivityHistory(String activity) {
        ValidatorHelper.validateEmptyString(activity, ACTIVITY_IS_EMPTY_ERROR_MESSAGE);
        String date = getDate();
        activityHistory.add(String.format(DATE_MESSAGE_FORMAT, date, activity));
    }

    private String getDate() {
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);

        return dateFormat.format(date);
    }
}
