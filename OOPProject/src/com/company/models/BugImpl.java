package com.company.models;

import com.company.models.contracts.*;
import com.company.models.enums.BugSevirity;
import com.company.models.enums.BugStatus;
import com.company.models.enums.Priority;
import com.company.models.validators.ValidatorHelper;

import java.util.ArrayList;
import java.util.List;

public class BugImpl extends WorkItemExtendedImpl<BugStatus> implements Bug {

    private static final String SEVERITY_FIELD = "Severity";
    private static final String STEPS_FIELD = "Steps to reproduce";
    private static final String EMPTY_STRING_ERROR_MESSAGE = "String cannot be empty.";

    private String stepsToReproduce;
    private BugSevirity severity;

    public BugImpl(Board board, String title, String description, BugStatus status, Priority priority,
                   Member member, BugSevirity severity, String stepsToReproduce) {
        super(board, title, description, status, priority, member);
        setSeverity(severity);
        setStepsToReproduce(stepsToReproduce);
    }

    @Override
    public String getStepsToReproduce() {
        return stepsToReproduce;
    }

    @Override
    public BugSevirity getBugSeverity() {
        return severity;
    }

    @Override
    public void setSeverity(BugSevirity severity) {
        this.severity = severity;
    }

    @Override
    protected String printAdditionalInfo() {
        return super.printAdditionalInfo() + String.format("%s: %s", SEVERITY_FIELD, getBugSeverity())
                + System.lineSeparator() + String.format("%s: %s", STEPS_FIELD, getStepsToReproduce())
                + System.lineSeparator();
    }

    private void setStepsToReproduce(String stepsToReproduce) {
        ValidatorHelper.validateEmptyString(stepsToReproduce, EMPTY_STRING_ERROR_MESSAGE);
        this.stepsToReproduce = stepsToReproduce;
    }
}
