package com.company.models;

import com.company.models.contracts.ActivityHistory;
import com.company.models.contracts.Board;
import com.company.models.contracts.WorkItem;
import com.company.models.validators.ValidatorHelper;

import java.util.ArrayList;
import java.util.List;

public class BoardImpl implements Board {

    private static final int NAME_MIN_LENGTH = 5;
    private static final int NAME_MAX_LENGTH = 15;
    private static String NAME_LENGTH_ERROR_MASSAGE_FORMAT =
            String.format("Name's length cannot be less than %d or more than %d symbols long.",
            NAME_MIN_LENGTH, NAME_MAX_LENGTH);
    private static final String NAME_IS_EMPTY_ERROR_MESSAGE = "Name cannot be empty.";
    private static final String ACTIVITY_IS_EMPTY_ERROR_MESSAGE = "Activity cannot be empty.";
    private static final String NO_ACTIVITY_MESSAGE = "----- No activity history -----";
    private static final String VALIDATE_EMPTY_LIST_MESSAGE = "List cannot be empty.";
    private static final String BOARD_ACTIVITY_SEPARATOR = "----- Board's activity history -----";
    private final static String BOARD_TO_STRING = "Board's name: %s";
    private static final String WORKITEM_HEADER = "----- Work Items ----- ";
    private static final String NO_WORKITEMS = "----- No work items -----";
    private static final String SEPARATOR = "********************";

    private String name;
    private List<WorkItem> workItems;
    private ActivityHistory activityHistory;

    public BoardImpl(String name) {
        setName(name);
        this.workItems = new ArrayList<>();
        this.activityHistory = new ActivityHistoryImpl();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<WorkItem> workItems() {
        return new ArrayList<>(workItems);
    }

    @Override
    public void addActivity(String activity) {
        ValidatorHelper.validateEmptyString(activity, ACTIVITY_IS_EMPTY_ERROR_MESSAGE);
        activityHistory.addActivityHistory(activity);
    }

    @Override
    public String showActivityHistory() {
        StringBuilder activity = new StringBuilder();

        if(activityHistory.getActivityHistory().isEmpty()) {
            activity.append(NO_ACTIVITY_MESSAGE).append(System.lineSeparator());
        }
        else {
            activity.append(BOARD_ACTIVITY_SEPARATOR)
                    .append(System.lineSeparator());

            for (String myString : activityHistory.getActivityHistory()) {
                activity.append(myString).append(System.lineSeparator());
            }
        }

        return activity.toString();
    }

    @Override
    public void addWorkItem(WorkItem workItem) {
        workItems.add(workItem);
    }

    @Override
    public String toString() {
        return SEPARATOR + System.lineSeparator() + String.format(BOARD_TO_STRING, getName())
                + System.lineSeparator() + printWorkItems()
                + showActivityHistory() + SEPARATOR + System.lineSeparator();
    }

    private void setName(String name) {
        ValidatorHelper.validateEmptyString(name, NAME_IS_EMPTY_ERROR_MESSAGE);
        ValidatorHelper.validateIntRange(name.length(), NAME_MIN_LENGTH,
                NAME_MAX_LENGTH, NAME_LENGTH_ERROR_MASSAGE_FORMAT);
        this.name = name;
    }

    private void setWorkItems(List<WorkItem> workItems) {
        if(workItems.isEmpty()) {
            throw new IllegalArgumentException(VALIDATE_EMPTY_LIST_MESSAGE);
        }
        this.workItems = workItems;
    }

    private String printWorkItems() {
        StringBuilder stringBuilder = new StringBuilder();

        if (workItems.isEmpty()) {
            stringBuilder.append(String.format("%s", NO_WORKITEMS))
                    .append(System.lineSeparator());
        } else {
            stringBuilder.append(String.format("%s", WORKITEM_HEADER)).
                    append(System.lineSeparator());

            for (WorkItem workItem : workItems) {
                stringBuilder.append(workItem.toString())
                        .append(System.lineSeparator());
            }
        }
        return stringBuilder.toString();
    }
}
