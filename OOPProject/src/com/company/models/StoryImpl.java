package com.company.models;

import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Story;
import com.company.models.enums.Priority;
import com.company.models.enums.StorySize;
import com.company.models.enums.StoryStatus;

public class StoryImpl extends WorkItemExtendedImpl<StoryStatus> implements Story {

    private static final String SIZE_FIELD = "Size";

    private StorySize storySize;

    public StoryImpl(Board board, String title, String description, StoryStatus status, Priority priority, Member member, StorySize storySize) {
        super(board, title, description, status, priority, member);
        setStorySize(storySize);
    }

    @Override
    public StorySize getStorySize() {
        return storySize;
    }

    @Override
    public void setStorySize(StorySize storySize) {
        this.storySize = storySize;
    }

    @Override
    protected String printAdditionalInfo() {
        return super.printAdditionalInfo() +
                String.format("%s: %s", SIZE_FIELD, getStorySize())
                + System.lineSeparator();
    }
}
