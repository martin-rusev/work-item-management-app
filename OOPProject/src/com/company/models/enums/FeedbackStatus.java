package com.company.models.enums;

import com.company.models.contracts.Status;

public enum FeedbackStatus implements Status {
    NEW,
    UNSCHEDULED,
    SCHEDULED,
    DONE;

    @Override
    public String toString() {
        return this.name().substring(0, 1).toUpperCase() + this.name().substring(1).toLowerCase();
    }
}
