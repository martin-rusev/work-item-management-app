package com.company.models.enums;

import com.company.models.contracts.Status;

public enum StoryStatus implements Status {
    NOTDONE,
    INPROGRESS,
    DONE;

    @Override
    public String toString() {
        return this.name().substring(0, 1).toUpperCase() + this.name().substring(1).toLowerCase();
    }
}
