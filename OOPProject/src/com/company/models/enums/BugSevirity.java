package com.company.models.enums;

public enum BugSevirity {
    CRITICAL,
    MAJOR,
    MINOR;

    @Override
    public String toString() {
        return this.name().substring(0, 1).toUpperCase() + this.name().substring(1).toLowerCase();
    }
}