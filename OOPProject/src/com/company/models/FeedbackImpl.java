package com.company.models;

import com.company.models.contracts.Board;
import com.company.models.contracts.Feedback;
import com.company.models.enums.FeedbackStatus;

public class FeedbackImpl extends WorkItemImpl<FeedbackStatus> implements Feedback {
    private static final String RATING_FIELD = "Rating";

    private int rating;

    public FeedbackImpl(Board board, String title, String description, FeedbackStatus status, int rating) {
        super(board, title, description, status);
        setRating(rating);
    }

    public int getRating() {
        return rating;
    }

    @Override
    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("%s: %d", RATING_FIELD, getRating())
                + System.lineSeparator();
    }
}
