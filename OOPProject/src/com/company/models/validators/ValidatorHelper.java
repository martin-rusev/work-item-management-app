package com.company.models.validators;

public class ValidatorHelper {

    public static void validateIntRange(int value, int min, int max, String message) {
        if (value < min || value > max) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateEmptyString(String myString, String message) {
        if (myString.isEmpty()) {
            throw new IllegalArgumentException(message);
        }
    }
}
