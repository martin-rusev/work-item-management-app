package com.company.models;

import com.company.models.contracts.*;
import com.company.models.validators.ValidatorHelper;

import java.util.ArrayList;
import java.util.List;

public abstract class WorkItemImpl<T extends Status> implements WorkItem {
    private static final int TITLE_MIN_LENGTH = 10;
    private static final int TITLE_MAX_LENGTH = 50;
    private static final String TITLE_LENGTH_ERROR_MESSAGE_FORMAT =
            String.format("Title's length cannot be less than %d or more than %d symbols long.",
                    TITLE_MIN_LENGTH, TITLE_MAX_LENGTH);
    private static final String TITLE_IS_EMPTY_ERROR_MESSAGE = "Title cannot be empty.";
    private static final String DESCRIPTION_IS_EMPTY_ERROR_MESSAGE = "Description cannot be empty.";

    private static final int DESCRIPTION_MIN_LENGTH = 10;
    private static final int DESCRIPTION_MAX_LENGTH = 500;
    private static final String DESCRIPTION_ERROR_MESSAGE_FORMAT =
            String.format("Description's length cannot be less than %d or more than %d symbols long.",
                    DESCRIPTION_MIN_LENGTH, DESCRIPTION_MAX_LENGTH);

    private static final String ID_FIELD = "ID";
    private static final String TITLE_FIELD = "Title";
    private static final String DESCRIPTION_FIELD = "Description";
    private static final String STATUS_FIELD = "Status";

    private static final String ACTIVITY_IS_EMPTY_ERROR_MESSAGE = "Activity cannot be empty.";

    private final static String COMMENTS_HEADER = "----- COMMENTS -----";
    private final static String NO_COMMENTS_HEADER = "----- NO COMMENTS -----";

    private final static String ACTIVE_HISTORY_HEADER = "------ ACTIVITY HISTORY -----";
    private final static String NO_ACTIVE_HISTORY_HEADER = "----- NO ACTIVITY HISTORY -----";

    private static final String SEPARATOR = "********************";

    private static int ID;
    private int workItemID;
    private String title;
    private String description;
    private Status status;
    private List<Comment> comments;
    private ActivityHistory activityHistory;
    private Board board;

    public WorkItemImpl(Board board, String title, String description, Status status) {
        this.board = board;
        setTitle(title);
        setDescription(description);

        ++ID;
        workItemID = ID;
        this.status = status;
        comments = new ArrayList<>();
        activityHistory = new ActivityHistoryImpl();
    }

    @Override
    public Board getBoard() {
        return board;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public void addActivity(String activity) {
        ValidatorHelper.validateEmptyString(activity, ACTIVITY_IS_EMPTY_ERROR_MESSAGE);
        activityHistory.addActivityHistory(activity);
    }

    @Override
    public void addComment(Comment comment) {
        comments.add(comment);
    }

    @Override
    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        String result = SEPARATOR + System.lineSeparator() + String
                .format("----- %s -----", this.getClass().getSimpleName().replace("Impl", ""))
                + System.lineSeparator() + String.format("%s: %d", ID_FIELD, getWorkItemId())
                + System.lineSeparator() + String.format("%s: %s", TITLE_FIELD, getTitle())
                + System.lineSeparator() + String.format("%s: %s", DESCRIPTION_FIELD, getDescription())
                + System.lineSeparator() + String.format("%s: %s", STATUS_FIELD, getStatus())
                + System.lineSeparator();

        if (!printAdditionalInfo().isEmpty()) {
           result += printAdditionalInfo() + System.lineSeparator();
        }

        result += printComments() + System.lineSeparator()
                + printActiveHistory();

        return result;
    }

    protected abstract String printAdditionalInfo();

    private int getWorkItemId() {
        return workItemID;
    }

    private void setTitle(String title) {
        ValidatorHelper.validateEmptyString(title, TITLE_IS_EMPTY_ERROR_MESSAGE);
        ValidatorHelper.validateIntRange(title.length(),
                TITLE_MIN_LENGTH, TITLE_MAX_LENGTH, TITLE_LENGTH_ERROR_MESSAGE_FORMAT);
        this.title = title;
    }

    private void setDescription(String description) {
        ValidatorHelper.validateEmptyString(description, DESCRIPTION_IS_EMPTY_ERROR_MESSAGE);
        ValidatorHelper.validateIntRange(description.length(),
                DESCRIPTION_MIN_LENGTH, DESCRIPTION_MAX_LENGTH, DESCRIPTION_ERROR_MESSAGE_FORMAT);
        this.description = description;
    }

    private String printComments() {
        StringBuilder builder = new StringBuilder();

        if (comments.isEmpty()) {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        } else {
            builder.append(String.format("%s", COMMENTS_HEADER))
                    .append(System.lineSeparator());

            for (Comment comment : comments) {
                builder.append(comment.toString());
            }
        }

        return builder.toString();
    }

    private String printActiveHistory() {
        StringBuilder builder = new StringBuilder();

        if (activityHistory.getActivityHistory().isEmpty()) {
            builder.append(String.format("%s", NO_ACTIVE_HISTORY_HEADER))
                    .append(System.lineSeparator());
        } else {
            builder.append(String.format("%s", ACTIVE_HISTORY_HEADER))
                    .append(System.lineSeparator());

            for (String myString : activityHistory.getActivityHistory()) {
                builder.append(myString).append(System.lineSeparator());
            }

            builder.append(System.lineSeparator());
        }

        return builder.toString();
    }
}
