package com.company.core.factories;

import com.company.core.contracts.WIMFactory;
import com.company.models.*;
import com.company.models.contracts.*;
import com.company.models.enums.*;

public class WIMFactoryImpl implements WIMFactory {

    @Override
    public Member createPerson(String memberName) {
        return new MemberImpl(memberName);
    }

    @Override
    public Board createBoard(String boardName) {
        return new BoardImpl(boardName);
    }

    @Override
    public Team createTeam(String teamName) {
        return new TeamImpl(teamName);
    }

    @Override
    public Bug createBug(Board board, String title, String description, String status, String priority,
                         Member member, String severity, String stepsToReproduce) {
        return new BugImpl(board, title, description, getBugStatus(status),
                getPriority(priority), member, getBugSeverity(severity), stepsToReproduce);
    }

    @Override
    public Story createStory(Board board, String title, String description, String status, String priority, Member member, String storySize) {
        return new StoryImpl(board, title, description, getStoryStatus(status), getPriority(priority), member, getStorySize(storySize));
    }

    @Override
    public Feedback createFeedback(Board board,String title, String description, String status, int rating) {
        return new FeedbackImpl(board, title, description, getFeedbackStatus(status), rating);
    }

    @Override
    public Comment createComment(String content, String author) {
        return new CommentImpl(content, author);
    }

    private BugStatus getBugStatus(String status) {
        return BugStatus.valueOf(status.toUpperCase());
    }

    private Priority getPriority(String priority) {
        return Priority.valueOf(priority.toUpperCase());
    }

    private BugSevirity getBugSeverity(String bugSeverity) {
        return BugSevirity.valueOf(bugSeverity.toUpperCase());
    }

    private StoryStatus getStoryStatus(String status) {
        return StoryStatus.valueOf(status.toUpperCase());
    }

    private StorySize getStorySize(String storySize) {
        return StorySize.valueOf(storySize.toUpperCase());
    }

    private FeedbackStatus getFeedbackStatus(String status) {
        return FeedbackStatus.valueOf(status.toUpperCase());
    }
}
