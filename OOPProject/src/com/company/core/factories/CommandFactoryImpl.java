package com.company.core.factories;

import com.company.commands.AssignBugToPerson;
import com.company.commands.AssignStoryToPerson;
import com.company.commands.UnAssignBugToPerson;
import com.company.commands.UnAssignStoryToPerson;
import com.company.commands.adding.AddComment;
import com.company.commands.adding.AddPersonToTeam;
import com.company.commands.change.*;
import com.company.commands.creating.*;
import com.company.commands.enums.CommandType;
import com.company.commands.listing.*;
import com.company.commands.showing.*;
import com.company.core.contracts.Command;
import com.company.core.contracts.CommandFactory;
import com.company.core.contracts.WIMFactory;
import com.company.core.contracts.WIMRepository;

public class CommandFactoryImpl  implements CommandFactory {

    private static final String INVALID_COMMAND = "Invalid command name: %s!";

    @Override
    public Command createCommand(String commandTypeAsString, WIMFactory wimFactory, WIMRepository wimRepository) {
        CommandType commandType = CommandType.valueOf(commandTypeAsString.toUpperCase());

        switch (commandType) {
            case ADDCOMMENT:
                return new AddComment(wimFactory, wimRepository);
            case ADDPERSONTOTEAM:
                return new AddPersonToTeam(wimRepository);
            case CHANGEBUGATTRIBUTES:
                return new ChangeBugAttributes(wimRepository);
            case CHANGEFEEDBACKATTRIBUTES:
                return new ChangeFeedbackAttributes(wimRepository);
            case CHANGESTORYATTRIBUTES:
                return new ChangeStoryAttributes(wimRepository);
            case CREATEBOARD:
                return new CreateBoard(wimFactory, wimRepository);
            case CREATEBUG:
                return new CreateBug(wimFactory, wimRepository);
            case CREATEFEEDBACK:
                return new CreateFeedback(wimFactory, wimRepository);
            case CREATEPERSON:
                return new CreatePerson(wimFactory, wimRepository);
            case CREATESTORY:
                return new CreateStory(wimFactory, wimRepository);
            case CREATETEAM:
                return new CreateTeam(wimFactory, wimRepository);
            case LISTALLWORKITEMS:
                return new ListAllWorkItems(wimRepository);
            case LISTBUGS:
                return new ListBugs(wimRepository);
            case LISTSTORIES:
                return new ListStories(wimRepository);
            case LISTFEEDBACK:
                return new ListFeedback(wimRepository);
            case LISTWORKITEMSBYFILTERASSIGNEE:
                return new ListWorkItemsByFilterAssignee(wimRepository);
            case LISTWORKITEMSBYFILTERSTATUS:
                return new ListWorkItemsByFilterStatus(wimRepository);
            case LISTWORKITEMSBYSORTPRIORITY:
                return new ListWorkItemsBySortPriority(wimRepository);
            case LISTWORKITEMSBYSORTRATING:
                return new ListWorkItemsBySortRating(wimRepository);
            case LISTWORKITEMSBYSORTSEVERITY:
                return new ListWorkItemsBySortSeverity(wimRepository);
            case LISTWORKITEMSBYSORTSIZE:
                return new ListWorkItemsBySortSize(wimRepository);
            case LISTWORKITEMSBYSORTTITLE:
                return new ListWorkItemsBySortTitle(wimRepository);
            case SHOWBOARDS:
                return new ShowBoards(wimRepository);
            case SHOWBOARDACTIVITY:
                return new ShowBoardActivity(wimRepository);
            case SHOWPEOPLE:
                return new ShowPeople(wimRepository);
            case SHOWPERSONACTIVITY:
                return new ShowPersonActivity(wimRepository);
            case SHOWTEAMACTIVITY:
                return new ShowTeamActivity(wimRepository);
            case SHOWTEAMMEMBERS:
                return new ShowTeamMembers(wimRepository);
            case SHOWTEAMS:
                return new ShowTeams(wimRepository);
            case ASSIGNBUGTOPERSON:
                return new AssignBugToPerson(wimRepository);
            case ASSIGNSTORYTOPERSON:
                return new AssignStoryToPerson(wimRepository);
            case UNASSIGNBUGTOPERSON:
                return new UnAssignBugToPerson(wimRepository);
            case UNASSIGNSTORYTOPERSON:
                return new UnAssignStoryToPerson(wimRepository);
        }
        throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandTypeAsString));
    }
}
