package com.company.core;

import com.company.core.contracts.*;
import com.company.core.factories.CommandFactoryImpl;
import com.company.core.factories.WIMFactoryImpl;
import com.company.core.providers.CommandParserImpl;
import com.company.core.providers.ConsoleReader;
import com.company.core.providers.ConsoleWriter;

import java.util.List;

public final class WIMEngineImpl implements Engine {

    private static final String TERMINATION_COMMAND = "Exit";

    private WIMFactory wimFactory;
    private WIMRepository wimRepository;
    private CommandParser commandParser;
    private CommandFactory commandFactory;
    private Writer writer;
    private Reader reader;


    public WIMEngineImpl() {
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        commandParser = new CommandParserImpl();
        commandFactory = new CommandFactoryImpl();
        writer = new ConsoleWriter();
        reader = new ConsoleReader();
    }

    @Override
    public void start() {
        while (true) {
            try {
                String commandAsString = reader.readLine();
                if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                processCommand(commandAsString);

            } catch (Exception ex) {
                writer.writeLine(ex.getMessage() != null && !ex.getMessage().isEmpty() ? ex.getMessage() : ex.toString());
            }
        }
    }

    private void processCommand(String commandAsString) {
        if (commandAsString == null || commandAsString.trim().equals("")) {
            throw new IllegalArgumentException("Command cannot be null or empty.");
        }

        String commandName = commandParser.parseCommand(commandAsString);
        Command command = commandFactory.createCommand(commandName, wimFactory, wimRepository);
        List<String> parameters = commandParser.parseParameters(commandAsString);
        String executionResult = command.execute(parameters);
        writer.writeLine(executionResult);
    }
}
