package com.company.core.contracts;

import com.company.models.contracts.*;
import com.company.models.enums.BugSevirity;
import com.company.models.enums.BugStatus;
import com.company.models.enums.Priority;

public interface WIMFactory {
    Member createPerson(String memberName);

    Board createBoard(String boardName);

    Team createTeam(String teamName);

    Bug createBug(Board board, String title, String description, String status, String priority,
                  Member member, String severity, String stepsToReproduce);

    Story createStory(Board board, String title, String description, String status, String priority,
                      Member member, String storySize);

    Feedback createFeedback(Board board, String title, String description, String status, int rating);

    Comment createComment(String content, String author);
}
