package com.company.core.contracts;

import com.company.models.contracts.*;

import java.util.Map;

public interface WIMRepository {

    Map<String, Member> getMembers();

    Map<String, Team> getTeams();

    Map<String, Board> getBoards();

    Map<String, Bug> getBugs();

    Map<String, Story> getStories();

    Map<String, Feedback> getFeedbacks();

    Map<String, WorkItem> getWorkItems();

    void addMember(String name, Member memberToAdd);

    void addTeam(String name, Team teamToAdd);

    void addBoard(String name, Board boardToAdd);

    void addBug(String name, Bug bugToAdd);

    void addStory(String name, Story storyToAdd);

    void addFeedback(String name, Feedback feedbackToAdd);

    void addWorkItem(String name, WorkItem workItem);
}
