package com.company.core.contracts;

public interface CommandFactory {
    Command createCommand(String commandTypeAsString, WIMFactory factory, WIMRepository repository);
}
