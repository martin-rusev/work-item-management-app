package com.company.core;

import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.*;

import java.util.HashMap;

import java.util.Map;

public class WIMRepositoryImpl implements WIMRepository {
    private Map<String, Board> boards;
    private Map<String, Member> members;
    private Map<String, Team> teams;
    private Map<String, Bug> bugs;
    private Map<String, Story> stories;
    private Map<String, Feedback> feedbacks;
    private Map<String, WorkItem> workItems;

    public WIMRepositoryImpl() {
        members = new HashMap<>();
        teams = new HashMap<>();
        boards = new HashMap<>();
        bugs = new HashMap<>();
        stories = new HashMap<>();
        feedbacks = new HashMap<>();
        workItems = new HashMap<>();
    }

    @Override
    public Map<String, Member> getMembers() {
        return new HashMap<>(members);
    }

    @Override
    public Map<String, Team> getTeams() {
        return new HashMap<>(teams);
    }

    @Override
    public Map<String, Board> getBoards() {
        return new HashMap<>(boards);
    }

    @Override
    public Map<String, Bug> getBugs() {
        return new HashMap<>(bugs);
    }

    @Override
    public Map<String, Story> getStories() {
        return new HashMap<>(stories);
    }

    @Override
    public Map<String, Feedback> getFeedbacks() {
        return new HashMap<>(feedbacks);
    }

    @Override
    public Map<String, WorkItem> getWorkItems() {
        return new HashMap<>(workItems);
    }

    @Override
    public void addMember(String name, Member memberToAdd) {
        members.put(name, memberToAdd);
    }

    @Override
    public void addTeam(String name, Team teamToAdd) {
        teams.put(name, teamToAdd);
    }

    @Override
    public void addBoard(String name, Board boardToAdd) {
        boards.put(name, boardToAdd);
    }

    @Override
    public void addBug(String name, Bug bugToAdd) {
        bugs.put(name, bugToAdd);
    }

    @Override
    public void addStory(String name, Story storyToAdd) {
        stories.put(name, storyToAdd);
    }

    @Override
    public void addFeedback(String name, Feedback feedbackToAdd) {
        feedbacks.put(name, feedbackToAdd);
    }

    @Override
    public void addWorkItem(String name, WorkItem workItem) {
        workItems.put(name, workItem);
    }
}
