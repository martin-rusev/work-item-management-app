package com.company.commands;

import com.company.commands.validatorHelper.CommandConstants;
import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;

import java.util.List;

public class UnAssignBugToPerson implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;
    private static final String UNASSIGNED_BUG_TO_PERSON = "Bug %s successfully unassign from member %s.";

    private final WIMRepository wimRepository;

    private String bug;
    private String person;

    public UnAssignBugToPerson(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkParametersSize(parameters.size(), CORRECT_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        CommandValidator.checkIfMemberExist(wimRepository, person);

        CommandValidator.checkIfBugExist(wimRepository, bug);

        return unassignBugToPerson(bug, person);
    }

    private String unassignBugToPerson(String bugName, String memberName) {
        Member member = wimRepository.getMembers().get(memberName);
        Bug bug = wimRepository.getBugs().get(bugName);
        bug.unAssignMember(memberName);

        String message = String.format(UNASSIGNED_BUG_TO_PERSON, bugName, memberName);
        member.addActivity(message);
        bug.addActivity(message);

        return message;
    }

    private void parseParameters(List<String> parameters) {
        try {
            bug = parameters.get(0);
            person = parameters.get(1);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(CommandConstants.FAILED_TO_PARSE_PARAMETERS);
        }
    }
}



