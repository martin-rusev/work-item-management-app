package com.company.commands.validatorHelper;

import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Board;
import com.company.models.contracts.Story;
import com.company.models.contracts.Team;
import com.company.models.contracts.WorkItem;

import java.util.List;
import java.util.Map;

public class CommandValidator {

    public static void validateWorkItems(List list) {
        if(list.isEmpty()) {
            throw new IllegalArgumentException(CommandConstants.IS_EMPTY_WORK_ITEMS_MESSAGE);
        }
    }

    public static void checkIfMapIsEmpty(Map map, String message) {
        if(map.isEmpty()) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void checkIfListIsEmpty(List list, String message) {
        if(list.isEmpty()) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void checkParametersSize(int parametersSize, int correctNumberOfArguments) {
        if (parametersSize != correctNumberOfArguments) {
            throw new IllegalArgumentException(CommandConstants.INVALID_NUMBER_OF_ARGUMENTS);
        }
    }

    public static void checkIfMemberExist(WIMRepository repository, String memberName) {
        if (!repository.getMembers().containsKey(memberName)) {
            throw new IllegalArgumentException(String
                    .format(CommandConstants.MEMBER_NOT_EXIST_MESSAGE, memberName));
        }
    }

    public static void checkIfMemberAlreadyExist(WIMRepository repository, String memberName) {
        if (repository.getMembers().containsKey(memberName)) {
            throw new IllegalArgumentException(String
                    .format(CommandConstants.MEMBER_EXISTS_ERROR_MESSAGE, memberName));
        }
    }

    public static void checkIfMemberAlreadyExistInTeam(Team team, String memberName, String teamName) {
        if (team.getMembers().contains(memberName)) {
            throw new IllegalArgumentException(String
                    .format(CommandConstants.MEMBER_ALREADY_IS_ADDED_ERROR_MESSAGE, memberName, teamName));
        }
    }

    public static void checkIfBoardExist(WIMRepository repository, String boardName) {
        if (!repository.getBoards().containsKey(boardName)) {
            throw new IllegalArgumentException(String
                    .format(CommandConstants.BOARD_NOT_EXIST_ERROR_MESSAGE, boardName));
        }
    }

    public static void checkIfBoardAlreadyExist(WIMRepository repository, String boardName) {
        if (repository.getBoards().containsKey(boardName)) {
            throw new IllegalArgumentException(String
                    .format(CommandConstants.BOARD_EXISTS_ERROR_MESSAGE, boardName));
        }
    }

    public static void checkIfTeamExist(WIMRepository repository, String teamName) {
        if (!repository.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException(String
                    .format(CommandConstants.TEAM_NOT_EXIST_ERROR_MESSAGE, teamName));
        }
    }

    public static void checkIfTeamAlreadyExist(WIMRepository repository, String teamName) {
        if (repository.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException(String
                    .format(CommandConstants.TEAM_EXISTS_ERROR_MESSAGE, teamName));
        }
    }

    public static void checkIfTeamListIsEmpty(WIMRepository repository) {
        if (repository.getTeams().isEmpty()) {
            throw new IllegalArgumentException(CommandConstants.TEAM_LIST_EMPTY_ERROR_MESSAGE);
        }
    }

    public static void checkIfBugExist(WIMRepository repository, String bugName) {
        if (!repository.getBugs().containsKey(bugName)) {
            throw new IllegalArgumentException(String
                    .format(CommandConstants.BUG_NOT_EXIST_MESSAGE, bugName));
        }
    }

    public static void checkIfBugAlreadyExist(WIMRepository repository, String bugName) {
        if(repository.getBugs().containsKey(bugName)) {
            throw new IllegalArgumentException(String
                    .format(CommandConstants.BUG_EXISTS_ERROR_MESSAGE_FORMAT, bugName));
        }
    }

    public static void checkIfStoryExist(WIMRepository repository, String storyName) {
        if (!repository.getStories().containsKey(storyName)) {
            throw new IllegalArgumentException(String
                    .format(CommandConstants.STORY_NOT_EXIST_MESSAGE, storyName));
        }
    }

    public static void checkIfStoryAlreadyExist(WIMRepository repository, String storyName) {
        if(repository.getStories().containsKey(storyName)) {
            throw new IllegalArgumentException(String
                    .format(CommandConstants.STORY_EXISTS_ERROR_MESSAGE, storyName));
        }
    }

    public static void checkIfFeedbackExist(WIMRepository repository, String feedbackName) {
        if (!repository.getFeedbacks().containsKey(feedbackName)) {
            throw new IllegalArgumentException(String
                    .format(CommandConstants.FEEDBACK_NOT_EXIST_MESSAGE, feedbackName));
        }
    }

    public static void checkIfFeedbackAlreadyExist(WIMRepository repository, String feedbackName) {
        if(repository.getFeedbacks().containsKey(feedbackName)) {
            throw new IllegalArgumentException(String
                    .format(CommandConstants.FEEDBACK_EXISTS_ERROR_MESSAGE, feedbackName));
        }
    }
}
