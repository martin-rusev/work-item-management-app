package com.company.commands.validatorHelper;

public class CommandConstants {

    static final String IS_EMPTY_WORK_ITEMS_MESSAGE = "There are no work items.";

    static final String MEMBER_NOT_EXIST_MESSAGE = "Member %s does not exist.";

    static final String MEMBER_EXISTS_ERROR_MESSAGE = "Member %s already exists.";

    static final String MEMBER_ALREADY_IS_ADDED_ERROR_MESSAGE = "Member %s is already added to the team %s.";

    static final String TEAM_NOT_EXIST_ERROR_MESSAGE = "Team %s does not exist.";

    static final String TEAM_EXISTS_ERROR_MESSAGE = "Team %s already exists.";

    static final String TEAM_LIST_EMPTY_ERROR_MESSAGE = "There are not existing teams.";

    static final String BUG_NOT_EXIST_MESSAGE= "Bug %s does not exist.";

    static final String BUG_EXISTS_ERROR_MESSAGE_FORMAT = "Bug %s already exists.";

    static final String STORY_NOT_EXIST_MESSAGE= "Story %s does not exist.";

    static final String STORY_EXISTS_ERROR_MESSAGE = "Story %s already exists.";

    static final String FEEDBACK_NOT_EXIST_MESSAGE= "Feedback %s does not exist";

    static final String FEEDBACK_EXISTS_ERROR_MESSAGE = "Feedback %s already exists.";

    static final String BOARD_NOT_EXIST_ERROR_MESSAGE = "Board %s does not exist.";

    static final String BOARD_EXISTS_ERROR_MESSAGE = "Board %s already exists.";

    static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments.";

    public static final String FAILED_TO_PARSE_PARAMETERS = "Failed to parse parameters.";
}
