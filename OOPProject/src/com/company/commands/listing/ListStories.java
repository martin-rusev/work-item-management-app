package com.company.commands.listing;

import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Story;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ListStories implements Command {

    private static final String EMPTY_STORY_LIST_MESSAGE = "There are no registered stories.";

    private WIMRepository wimRepository;

    public ListStories(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        List<Story> storyList = new ArrayList<>(wimRepository.getStories().values());

        CommandValidator.checkIfListIsEmpty(storyList, EMPTY_STORY_LIST_MESSAGE);

        return storyList.stream()
                .map(String::valueOf).collect(Collectors.joining());
    }
}
