package com.company.commands.listing;

import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Bug;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ListWorkItemsBySortSeverity implements Command {
    private final WIMRepository wimRepository;

    public ListWorkItemsBySortSeverity(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        List<Bug> list = new ArrayList<>(wimRepository.getBugs().values());

        CommandValidator.validateWorkItems(list);

        List<Bug> sortedList = list.stream().
                sorted(Comparator.comparing(Bug::getBugSeverity)).
                collect(Collectors.toList());

        return sortedList.stream()
                .map(String::valueOf).collect(Collectors.joining());
    }
}
