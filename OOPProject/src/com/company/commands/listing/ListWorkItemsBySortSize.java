package com.company.commands.listing;

import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Story;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ListWorkItemsBySortSize implements Command {
    private final WIMRepository wimRepository;

    public ListWorkItemsBySortSize(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        List<Story> list = new ArrayList<>(wimRepository.getStories().values());

        CommandValidator.validateWorkItems(list);

        List<Story> sortedList = list.stream().
                sorted(Comparator.comparing(Story::getStorySize)).
                collect(Collectors.toList());

        return sortedList.stream()
                .map(String::valueOf).collect(Collectors.joining());
    }
}
