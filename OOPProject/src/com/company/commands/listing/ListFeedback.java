package com.company.commands.listing;

import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Feedback;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ListFeedback implements Command {

    private static final String EMPTY_FEEDBACK_LIST_MESSAGE = "There is no registered feedback.";

    private WIMRepository wimRepository;

    public ListFeedback(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        List<Feedback> feedbackList = new ArrayList<>(wimRepository.getFeedbacks().values());

        CommandValidator.checkIfListIsEmpty(feedbackList, EMPTY_FEEDBACK_LIST_MESSAGE);

        return feedbackList.stream()
                .map(String::valueOf).collect(Collectors.joining());
    }
}
