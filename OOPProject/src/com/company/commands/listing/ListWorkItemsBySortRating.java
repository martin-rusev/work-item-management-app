package com.company.commands.listing;

import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Feedback;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ListWorkItemsBySortRating implements Command {
    private final WIMRepository wimRepository;

    public ListWorkItemsBySortRating(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        List<Feedback> list = new ArrayList<>(wimRepository.getFeedbacks().values());

        CommandValidator.validateWorkItems(list);

        List<Feedback> sortedList = list.stream()
                .sorted(Comparator.comparing(Feedback::getRating))
                .collect(Collectors.toList());

        return sortedList.stream()
                .map(String::valueOf).collect(Collectors.joining());
    }
}