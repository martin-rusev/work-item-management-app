package com.company.commands.listing;

import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Story;
import com.company.models.contracts.WorkItemExtended;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ListWorkItemsBySortPriority implements Command {
    private final WIMRepository wimRepository;

    public ListWorkItemsBySortPriority(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        List<WorkItemExtended> list = new ArrayList<>();

        List<Bug> bugList = new ArrayList<>(wimRepository.getBugs().values());
        List<Story> storyList = new ArrayList<>(wimRepository.getStories().values());

        list.addAll(bugList);
        list.addAll(storyList);

        CommandValidator.validateWorkItems(list);

        List<WorkItemExtended> sortedList = list.stream().
                sorted(Comparator.comparing(WorkItemExtended::getPriority)).
                collect(Collectors.toList());

        return sortedList.stream()
                .map(String::valueOf).collect(Collectors.joining());
    }
}

