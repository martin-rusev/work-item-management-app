package com.company.commands.listing;

import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Bug;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ListBugs implements Command {

    private static final String EMPTY_BUG_LIST_MESSAGE = "There are no registered bugs.";

    private final WIMRepository wimRepository;

    public ListBugs(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        List<Bug> bugList = new ArrayList<>(wimRepository.getBugs().values());

        CommandValidator.checkIfListIsEmpty(bugList, EMPTY_BUG_LIST_MESSAGE);

        return bugList.stream()
                .map(String::valueOf).collect(Collectors.joining());
    }
}


