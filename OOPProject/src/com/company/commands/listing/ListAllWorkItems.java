package com.company.commands.listing;

import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Feedback;
import com.company.models.contracts.Story;
import com.company.models.contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ListAllWorkItems implements Command {

    private final String NO_WORK_ITEMS_MESSAGE = "There are no added work items.";

    private final WIMRepository wimRepository;

    public ListAllWorkItems( WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        List<WorkItem> list = new ArrayList<>();

        List<Bug> bugList = new ArrayList<>(wimRepository.getBugs().values());
        List<Story> storyList = new ArrayList<>(wimRepository.getStories().values());
        List<Feedback> feedbackList = new ArrayList<>(wimRepository.getFeedbacks().values());

        list.addAll(bugList);
        list.addAll(storyList);
        list.addAll(feedbackList);

        CommandValidator.checkIfListIsEmpty(list, NO_WORK_ITEMS_MESSAGE);

        return list.stream()
                .map(String::valueOf).collect(Collectors.joining());
    }
}




