package com.company.commands.listing;

import com.company.commands.validatorHelper.CommandConstants;
import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Status;
import com.company.models.contracts.WorkItem;
import com.company.models.enums.BugStatus;
import com.company.models.enums.FeedbackStatus;
import com.company.models.enums.StoryStatus;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ListWorkItemsByFilterStatus implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;
    private static final String FILTERED_STATUS_ERROR_MESSAGE = "There are no work items with status %s.";

    private final WIMRepository wimRepository;

    private  String searchedStatusAsText;

    public ListWorkItemsByFilterStatus(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkParametersSize(parameters.size(),
                CORRECT_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        return filterStatus(searchedStatusAsText.toUpperCase());
    }

    private String filterStatus(String searchedStatusAsText) {
        Status searchedStatus;

        boolean isBugStatus = Stream.of(BugStatus.values())
                .map(BugStatus::name)
                .collect(Collectors.toList()).contains(searchedStatusAsText);
        boolean isFeedbackStatus = Stream.of(FeedbackStatus.values())
                .map(FeedbackStatus::name)
                .collect(Collectors.toList()).contains(searchedStatusAsText);

        if(isBugStatus){
            searchedStatus = BugStatus.valueOf(searchedStatusAsText);
        }else if(isFeedbackStatus){
            searchedStatus = FeedbackStatus.valueOf(searchedStatusAsText);
        } else {
            searchedStatus = StoryStatus.valueOf(searchedStatusAsText);
        }

        List<String> filteredWorkItemTitles = wimRepository.getWorkItems().values().stream()
                .filter((workItem) -> workItem.getStatus() == searchedStatus)
                .map(WorkItem::getTitle)
                .collect(Collectors.toList());

        CommandValidator.checkIfListIsEmpty(filteredWorkItemTitles,
                String.format(FILTERED_STATUS_ERROR_MESSAGE, searchedStatusAsText));

        return String.format("----- Work items with status %s -----", searchedStatusAsText.toUpperCase())
                + System.lineSeparator() + String.join(System.lineSeparator(), filteredWorkItemTitles)
                + System.lineSeparator();
    }

    private void parseParameters(List<String> parameters) {
        try {
            searchedStatusAsText = parameters.get(0);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(CommandConstants.FAILED_TO_PARSE_PARAMETERS);
        }
    }
}
