package com.company.commands.listing;

import com.company.commands.validatorHelper.CommandConstants;
import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Member;
import com.company.models.contracts.WorkItem;

import java.util.List;

public class ListWorkItemsByFilterAssignee implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;
    private static final String NO_ASSIGNEE_ERROR = "There are no assignees to the team.";


    private final WIMRepository wimRepository;

    private StringBuilder stringBuilder;
    private String searchedMemberAsText;

    public ListWorkItemsByFilterAssignee(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
        this.stringBuilder = new StringBuilder();
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkParametersSize(parameters.size(),
                CORRECT_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        return listAssignee(searchedMemberAsText);
    }

    private String listAssignee(String searchedMemberAsText) {

        Member member = wimRepository.getMembers().get(searchedMemberAsText);
        for (WorkItem workItem : member.workItems()) {
            stringBuilder.append(workItem.toString());
            stringBuilder.append(System.lineSeparator());
        }
        if (stringBuilder.toString().isEmpty()) {
            return NO_ASSIGNEE_ERROR;
        }

        return stringBuilder.toString();
    }

    private void parseParameters(List<String> parameters) {
        try {
            searchedMemberAsText = parameters.get(0);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(CommandConstants.FAILED_TO_PARSE_PARAMETERS);
        }
    }
}


