package com.company.commands.creating;

import com.company.commands.validatorHelper.CommandConstants;
import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMFactory;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Team;

import java.util.List;

public class CreateTeam implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;
    private static final String TEAM_CREATED_SUCCESS_MESSAGE = "Team %s successfully created.";

    private final WIMFactory wimFactory;
    private final WIMRepository wimRepository;

    private String team;

    public CreateTeam(WIMFactory wimFactory, WIMRepository wimRepository) {
        this.wimFactory = wimFactory;
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkParametersSize(parameters.size(), CORRECT_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        return createTeam(team);
    }

    private String createTeam(String teamName) {
        CommandValidator.checkIfTeamAlreadyExist(wimRepository, teamName);

        Team team = wimFactory.createTeam(teamName);
        wimRepository.addTeam(teamName, team);

        String message = String.format(TEAM_CREATED_SUCCESS_MESSAGE, teamName);
        team.addActivity(message);

        return message;
    }

    private void parseParameters(List<String> parameters) {
        try {
            team = parameters.get(0);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(CommandConstants.FAILED_TO_PARSE_PARAMETERS);
        }
    }
}
