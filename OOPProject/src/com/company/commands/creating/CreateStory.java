package com.company.commands.creating;

import com.company.commands.validatorHelper.CommandConstants;
import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMFactory;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Story;

import java.util.List;

public class CreateStory implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 7;
    private static final String STORY_CREATED_SUCCESS_MESSAGE = "Story %s successfully created in board %s.";

    private final WIMFactory wimFactory;
    private final WIMRepository wimRepository;

    private Board board;
    private String title;
    private String description;
    private String status;
    private String priority;
    private Member member;
    private String storySize;

    public CreateStory(WIMFactory wimFactory, WIMRepository wimRepository) {
        this.wimFactory = wimFactory;
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkParametersSize(parameters.size(), CORRECT_NUMBER_OF_ARGUMENTS);

        CommandValidator.checkIfBoardExist(wimRepository, parameters.get(0));

        CommandValidator.checkIfMemberExist(wimRepository, parameters.get(5));

        parseParameters(parameters);

        return createStory(board, title, description, status, priority, member, storySize);
    }

    private String createStory(Board board, String storyName, String description, String status, String priority, Member member, String size) {
        CommandValidator.checkIfStoryAlreadyExist(wimRepository, storyName);

        Story story = wimFactory.createStory(board, storyName, description, status, priority, member, size);

        board.addWorkItem(story);
        wimRepository.addStory(storyName, story);
        wimRepository.addWorkItem(storyName, story);

        String message = String.format(STORY_CREATED_SUCCESS_MESSAGE, storyName, board.getName());
        story.addActivity(message);
        board.addActivity(message);

        return message;
    }

    private void parseParameters(List<String> parameters) {
        try {
            board = wimRepository.getBoards().get(parameters.get(0));
            title = parameters.get(1);
            description = parameters.get(2);
            status = parameters.get(3);
            priority = parameters.get(4);
            member = wimRepository.getMembers().get(parameters.get(5));
            storySize = parameters.get(6);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(CommandConstants.FAILED_TO_PARSE_PARAMETERS);
        }
    }
}