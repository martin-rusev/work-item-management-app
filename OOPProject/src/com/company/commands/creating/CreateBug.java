package com.company.commands.creating;

import com.company.commands.validatorHelper.CommandConstants;
import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMFactory;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;

import java.util.List;

public class CreateBug implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 8;
    private static final String BUG_CREATED_SUCCESS_MESSAGE_FORMAT = "Bug %s successfully created in board %s.";

    private final WIMFactory wimFactory;
    private final WIMRepository wimRepository;

    private Board board;
    private String title;
    private String description;
    private String status;
    private String priority;
    private Member member;
    private String severity;
    private String stepsToReproduce;

    public CreateBug(WIMFactory wimFactory, WIMRepository wimRepository) {
        this.wimFactory = wimFactory;
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkParametersSize(parameters.size(), CORRECT_NUMBER_OF_ARGUMENTS);

        CommandValidator.checkIfBoardExist(wimRepository, parameters.get(0));
        CommandValidator.checkIfMemberExist(wimRepository, parameters.get(5));

       parseParameters(parameters);

        return createBug(board, title, description, status, priority, member, severity, stepsToReproduce);
    }

    private String createBug(Board board, String bugName, String description, String status, String priority,
                             Member member, String severity, String stepsToReproduce) {
       CommandValidator.checkIfBugAlreadyExist(wimRepository, bugName);

        Bug bug = wimFactory.createBug(board, bugName, description, status,
                priority, member, severity, stepsToReproduce);

        board.addWorkItem(bug);
        wimRepository.addBug(bugName, bug);
        wimRepository.addWorkItem(bugName, bug);

        String message = String.format(BUG_CREATED_SUCCESS_MESSAGE_FORMAT,
                bugName, board.getName());
        board.addActivity(message);
        bug.addActivity(message);

        return message;
    }

    private void parseParameters(List<String> parameters) {
        try {
            board = wimRepository.getBoards().get(parameters.get(0));
            title = parameters.get(1);
            description = parameters.get(2);
            status = parameters.get(3);
            priority = parameters.get(4);
            member = wimRepository.getMembers().get(parameters.get(5));
            severity = parameters.get(6);
            stepsToReproduce = parameters.get(7);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(CommandConstants.FAILED_TO_PARSE_PARAMETERS);
        }
    }
}

