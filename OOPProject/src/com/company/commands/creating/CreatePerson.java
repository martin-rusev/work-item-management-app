package com.company.commands.creating;

import com.company.commands.validatorHelper.CommandConstants;
import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMFactory;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Member;

import java.util.List;

public class CreatePerson implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;
    private static final String PERSON_CREATED_SUCCESS_MESSAGE = "Person %s successfully created.";

    private final WIMRepository wimRepository;
    private final WIMFactory wimFactory;

    private String member;

    public CreatePerson(WIMFactory wimFactory, WIMRepository wimRepository) {
        this.wimFactory = wimFactory;
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkParametersSize(parameters.size(), CORRECT_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        return createPerson(member);
    }

    private String createPerson(String memberName) {
        CommandValidator.checkIfMemberAlreadyExist(wimRepository, memberName);

        Member member = wimFactory.createPerson(memberName);
        wimRepository.addMember(memberName, member);

        String message = String.format(PERSON_CREATED_SUCCESS_MESSAGE, memberName);
        member.addActivity(message);

        return message;
    }

    private void parseParameters(List<String> parameters) {
        try {
            member = parameters.get(0);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(CommandConstants.FAILED_TO_PARSE_PARAMETERS);
        }
    }
}

