package com.company.commands.creating;

import com.company.commands.validatorHelper.CommandConstants;
import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMFactory;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Board;
import com.company.models.contracts.Team;

import java.util.List;

public class CreateBoard implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;
    private static final String BOARD_CREATED_SUCCESS_MESSAGE = "Board %s successfully created in team %s.";

    private final WIMFactory wimFactory;
    private final WIMRepository wimRepository;

    private String team;
    private String board;

    public CreateBoard(WIMFactory wimFactory, WIMRepository wimRepository) {
        this.wimFactory = wimFactory;
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkParametersSize(parameters.size(), CORRECT_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        CommandValidator.checkIfTeamExist(wimRepository, team);

        return createBoard(team, board);
    }

    private String createBoard(String teamName, String boardName) {
        CommandValidator.checkIfBoardAlreadyExist(wimRepository, boardName);

        Team team = wimRepository.getTeams().get(teamName);
        Board board = wimFactory.createBoard(boardName);

        team.addBoard(board);
        wimRepository.addBoard(boardName, board);

        String message = String.format(BOARD_CREATED_SUCCESS_MESSAGE, boardName, teamName);
        board.addActivity(message);
        team.addActivity(message);

        return message;
    }

    private void parseParameters(List<String> parameters) {
        try {
            team = parameters.get(0);
            board = parameters.get(1);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(CommandConstants.FAILED_TO_PARSE_PARAMETERS);
        }
    }
}
