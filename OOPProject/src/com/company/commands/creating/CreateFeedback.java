package com.company.commands.creating;

import com.company.commands.validatorHelper.CommandConstants;
import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMFactory;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Board;
import com.company.models.contracts.Feedback;

import java.util.List;

public class CreateFeedback implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;
    private static final String FEEDBACK_CREATED_SUCCESS_MESSAGE = "Feedback %s successfully created in board %s.";

    private final WIMFactory wimFactory;
    private final WIMRepository wimRepository;

    private Board board;
    private String title;
    private String description;
    private String status;
    private int rating;

    public CreateFeedback(WIMFactory wimFactory, WIMRepository wimRepository) {
        this.wimFactory = wimFactory;
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkParametersSize(parameters.size(), CORRECT_NUMBER_OF_ARGUMENTS);

        CommandValidator.checkIfBoardExist(wimRepository, parameters.get(0));

        parseParameters(parameters);

        return createFeedback(board, title, description, status, rating);
    }

    private String createFeedback(Board board, String feedbackName, String description, String status, int rating) {
       CommandValidator.checkIfFeedbackAlreadyExist(wimRepository, feedbackName);

        Feedback feedback = wimFactory.createFeedback(board, feedbackName, description, status, rating);

        board.addWorkItem(feedback);
        wimRepository.addFeedback(feedbackName, feedback);
        wimRepository.addWorkItem(feedbackName, feedback);

        String message = String.format(FEEDBACK_CREATED_SUCCESS_MESSAGE, feedbackName, board.getName());
        feedback.addActivity(message);
        board.addActivity(message);

        return message;
    }

    private void parseParameters(List<String> parameters) {
        try {
            board = wimRepository.getBoards().get(parameters.get(0));
            title = parameters.get(1);
            description = parameters.get(2);
            status = parameters.get(3);
            rating = Integer.parseInt(parameters.get(4));
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(CommandConstants.FAILED_TO_PARSE_PARAMETERS);
        }
    }
}
