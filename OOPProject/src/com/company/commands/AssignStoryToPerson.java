package com.company.commands;

import com.company.commands.validatorHelper.CommandConstants;
import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Member;
import com.company.models.contracts.Story;

import java.util.List;

public class AssignStoryToPerson implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;
    private static final String ASSIGN_STORY_TO_PERSON_MESSAGE = "Story %s successfully assign to member %s.";

    private final WIMRepository wimRepository;

    private String story;
    private String person;

    public AssignStoryToPerson(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkParametersSize(parameters.size(), CORRECT_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        CommandValidator.checkIfMemberExist(wimRepository, person);

        CommandValidator.checkIfStoryExist(wimRepository, story);

        return assignStoryToPerson(story, person);
    }

    private String assignStoryToPerson(String storyName, String memberName) {
        Member member = wimRepository.getMembers().get(memberName);
        Story story = wimRepository.getStories().get(storyName);
        story.assignMember(memberName);

        String message = String.format(ASSIGN_STORY_TO_PERSON_MESSAGE, storyName, memberName);
        member.addActivity(message);
        story.addActivity(message);

        member.addWorkItem(story);

        return message;
    }

    private void parseParameters(List<String> parameters) {
        try {
            story = parameters.get(0);
            person = parameters.get(1);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(CommandConstants.FAILED_TO_PARSE_PARAMETERS);
        }
    }
}
