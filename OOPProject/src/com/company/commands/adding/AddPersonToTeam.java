package com.company.commands.adding;

import com.company.commands.validatorHelper.CommandConstants;
import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;

import java.util.List;

public class AddPersonToTeam implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;
    private static final String PERSON_ADDED_SUCCESS_MESSAGE = "Person %s added successfully to team %s.";

    private final WIMRepository wimRepository;

    private String teamToAddedPerson;
    private String addPersonToTeam;

    public AddPersonToTeam(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkParametersSize(parameters.size(), CORRECT_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        CommandValidator.checkIfTeamExist(wimRepository, teamToAddedPerson);

        CommandValidator.checkIfMemberExist(wimRepository, addPersonToTeam);
        

        return addPersonToTeam(teamToAddedPerson, addPersonToTeam);
    }

    private String addPersonToTeam(String teamToAddedPerson, String addPersonToTeam) {
        Team team = wimRepository.getTeams().get(teamToAddedPerson);
        Member member = wimRepository.getMembers().get(addPersonToTeam);

        CommandValidator.checkIfMemberAlreadyExistInTeam(team,
                addPersonToTeam, teamToAddedPerson);

        String message = String.format(PERSON_ADDED_SUCCESS_MESSAGE,
                addPersonToTeam, teamToAddedPerson);

        team.addMember(member);
        team.addActivity(message);
        member.addActivity(message);

        return message;
    }

    private void parseParameters(List<String> parameters) {
        try {
            teamToAddedPerson = parameters.get(0);
            addPersonToTeam = parameters.get(1);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(CommandConstants.FAILED_TO_PARSE_PARAMETERS);
        }
    }
}
