package com.company.commands.adding;

import com.company.commands.validatorHelper.CommandConstants;
import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMFactory;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Comment;
import com.company.models.contracts.Member;
import com.company.models.contracts.WorkItem;

import java.util.List;

public class AddComment implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 3;
    private static final String WORKITEM_NOT_EXIST_MESSAGE = "The specified work item does not exist!";
    private static final String COMMENT_ADDED_MESSAGE = "Comment \"%s\" added successfully to %s by %s.";

    private final WIMFactory wimFactory;
    private final WIMRepository wimRepository;

    private String content;
    private String author;

    public AddComment(WIMFactory wimFactory, WIMRepository wimRepository) {
        this.wimFactory = wimFactory;
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkParametersSize(parameters.size(),
                CORRECT_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        CommandValidator.checkIfMemberExist(wimRepository, author);

        WorkItem workItem;
        if (wimRepository.getBugs().containsKey(parameters.get(2))) {
            workItem = wimRepository.getBugs().get(parameters.get(2));
        } else if (wimRepository.getStories().containsKey(parameters.get(2))) {
            workItem = wimRepository.getStories().get(parameters.get(2));
        } else if(wimRepository.getFeedbacks().containsKey(parameters.get(2))) {
            workItem = wimRepository.getFeedbacks().get(parameters.get(2));
        } else {
            throw new IllegalArgumentException(WORKITEM_NOT_EXIST_MESSAGE);
        }

        return addComment(content, author, workItem);

}

    private String addComment (String content, String author, WorkItem workItem) {
        Comment comment = wimFactory.createComment(content, author);
        workItem.addComment(comment);

        String message = String.format(COMMENT_ADDED_MESSAGE,
                comment.getContent(), workItem.getTitle(), author);

        workItem.addActivity(message);

        Member member = wimFactory.createPerson(author);
        wimRepository.addMember(author, member);
        member.addActivity(message);

        return message;
    }

    private void parseParameters(List<String> parameters) {
        try {
            content = parameters.get(0);
            author = parameters.get(1);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(CommandConstants.FAILED_TO_PARSE_PARAMETERS);
        }
    }
}
