package com.company.commands.showing;

import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;

import java.util.List;

public class ShowTeams implements Command {

    private static final String ALL_TEAMS_MESSAGE = "All teams: ";

    private final WIMRepository wimRepository;

    public ShowTeams(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        return shallAllTeams();
    }

    private String shallAllTeams() {
        CommandValidator.checkIfTeamListIsEmpty(wimRepository);

        return ALL_TEAMS_MESSAGE + wimRepository.getTeams().keySet().toString()
                .replace("[", "")
                .replace("]", "");
    }
}
