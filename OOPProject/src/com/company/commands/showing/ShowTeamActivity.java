package com.company.commands.showing;

import com.company.commands.validatorHelper.CommandConstants;
import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;

import java.util.List;

public class ShowTeamActivity implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;
    private static final String BOARD_EXIST_ERROR_MESSAGE = "Team %s does not exist";

    private final WIMRepository wimRepository;

    private String teamToShowActivity;

    public ShowTeamActivity(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkParametersSize(parameters.size(), CORRECT_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        return showTeamActivity(teamToShowActivity);
    }

    private String showTeamActivity(String teamToShowActivity) {
        CommandValidator.checkIfTeamExist(wimRepository, teamToShowActivity);

        return wimRepository.getTeams().get(teamToShowActivity).showActivityHistory();
    }

    private void parseParameters(List<String> parameters) {
        try {
            teamToShowActivity = parameters.get(0);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(CommandConstants.FAILED_TO_PARSE_PARAMETERS);
        }
    }
}