package com.company.commands.showing;

import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Member;

import java.util.ArrayList;
import java.util.List;

public class ShowPeople implements Command {

    private static final String SHOW_ALL_PEOPLE_EMPTY_ERROR_MESSAGE = "There are not existing members.";
    private static final String ALL_MEMBERS_MESSAGE = "All people: ";

    private final WIMRepository wimRepository;

    public ShowPeople(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        return shallAllPeople();
    }

    private String shallAllPeople() {
        List<Member> list = new ArrayList<>(wimRepository.getMembers().values());

        CommandValidator.checkIfListIsEmpty(list, SHOW_ALL_PEOPLE_EMPTY_ERROR_MESSAGE);

        return ALL_MEMBERS_MESSAGE + wimRepository.getMembers().keySet().toString()
                .replace("[", "")
                .replace("]", "");
    }
}

