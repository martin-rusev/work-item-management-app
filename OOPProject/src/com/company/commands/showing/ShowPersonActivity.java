package com.company.commands.showing;

import com.company.commands.validatorHelper.CommandConstants;
import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;

import java.util.List;

public class ShowPersonActivity implements Command {

    private  static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    private final WIMRepository wimRepository;

    private String memberToShowActivity;

    public ShowPersonActivity(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkParametersSize(parameters.size(), CORRECT_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        return showActivity(memberToShowActivity);
    }

    private String showActivity(String memberToShowActivity) {
        CommandValidator.checkIfMemberExist(wimRepository, memberToShowActivity);

        return wimRepository.getMembers().get(memberToShowActivity).showActivityHistory();
    }

    private void parseParameters(List<String> parameters) {
        try {
            memberToShowActivity = parameters.get(0);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(CommandConstants.FAILED_TO_PARSE_PARAMETERS);
        }
    }
}
