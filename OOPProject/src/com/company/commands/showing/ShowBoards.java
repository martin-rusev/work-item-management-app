package com.company.commands.showing;

import com.company.commands.validatorHelper.CommandConstants;
import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Team;

import java.util.List;

public class ShowBoards implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    private final WIMRepository wimRepository;

    private String teamName;

    public ShowBoards(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkParametersSize(parameters.size(), CORRECT_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        return showBoards(teamName);
    }

    private String showBoards(String teamName) {
        CommandValidator.checkIfTeamExist(wimRepository, teamName);

        Team team = wimRepository.getTeams().get(teamName);

        return team.printBoards();
    }

    private void parseParameters(List<String> parameters) {
        try {
            teamName = parameters.get(0);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(CommandConstants.FAILED_TO_PARSE_PARAMETERS);
        }
    }
}