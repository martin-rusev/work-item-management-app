package com.company.commands.change;

import com.company.commands.validatorHelper.CommandConstants;
import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Story;
import com.company.models.enums.Priority;
import com.company.models.enums.StorySize;
import com.company.models.enums.StoryStatus;

import java.util.List;

public class ChangeStoryAttributes implements Command {

    private static final String INVALID_COMMAND = "Invalid command name: %s!";

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 3;
    private static final String STORY_SET_MESSAGE_FORMAT = "%s of story %s has been changed to %s.";
    private static final String VALIDATE_ENUM_ERROR_MESSAGE = "Failed to set %s enum of story %s to %s.";

    private final WIMRepository wimRepository;

    private String storyTitle;
    private String state;
    private String newStoryAttribute;

    public ChangeStoryAttributes(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkParametersSize(parameters.size(), CORRECT_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        CommandValidator.checkIfStoryExist(wimRepository, storyTitle);

        Story story = wimRepository.getStories().get(storyTitle);

        String newState = state.toUpperCase();

        switch (newState) {
            case "PRIORITY":
                try {
                    story.setPriority(Priority.valueOf(newStoryAttribute.toUpperCase()));
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException(String.format(VALIDATE_ENUM_ERROR_MESSAGE, storyTitle, state, newStoryAttribute));
                }

                break;
            case "SIZE":
                try {
                    story.setStorySize(StorySize.valueOf(newStoryAttribute.toUpperCase()));
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException(String.format(VALIDATE_ENUM_ERROR_MESSAGE, storyTitle, state, newStoryAttribute));
                }

                break;
            case "STATUS":
                try {
                    story.setStatus(StoryStatus.valueOf(newStoryAttribute.toUpperCase()));
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException(String.format(VALIDATE_ENUM_ERROR_MESSAGE, storyTitle, state, newStoryAttribute));
                }

                break;
            default:
                throw new IllegalArgumentException(String.format(INVALID_COMMAND, state));
        }

        String message = String.format(STORY_SET_MESSAGE_FORMAT, storyTitle, state, newStoryAttribute);
        story.addActivity(message);

        return message;
    }

    private void parseParameters(List<String> parameters) {
        try {
            storyTitle = parameters.get(0);
            state = parameters.get(1);
            newStoryAttribute= parameters.get(2);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(CommandConstants.FAILED_TO_PARSE_PARAMETERS);
        }
    }
}
