package com.company.commands.change;

import com.company.commands.validatorHelper.CommandConstants;
import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Bug;
import com.company.models.enums.BugSevirity;
import com.company.models.enums.BugStatus;
import com.company.models.enums.Priority;

import java.util.List;

public class ChangeBugAttributes implements Command {

    private static final String INVALID_COMMAND = "Invalid command name: %s!";

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 3;
    private static final String BUG_SET_MESSAGE_FORMAT = "%s of bug %s has been changed to %s.";
    private static final String VALIDATE_ENUM_ERROR_MESSAGE = "Failed to set %s enum of bug %s to %s.";

    private final WIMRepository wimRepository;

    private String bugTitle;
    private String state;
    private String newBugAttribute;

    public ChangeBugAttributes(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkParametersSize(parameters.size(), CORRECT_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        CommandValidator.checkIfBugExist(wimRepository, bugTitle);
        Bug bug = wimRepository.getBugs().get(bugTitle);

        String newState = state.toUpperCase();

        switch (newState) {
            case "PRIORITY":
                try {
                    bug.setPriority(Priority.valueOf(newBugAttribute.toUpperCase()));
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException(String.format(VALIDATE_ENUM_ERROR_MESSAGE, bugTitle, state, newBugAttribute));
                }

                break;
            case "SEVERITY":
                try {
                    bug.setSeverity(BugSevirity.valueOf(newBugAttribute.toUpperCase()));
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException(String.format(VALIDATE_ENUM_ERROR_MESSAGE, bugTitle, state, newBugAttribute));
                }

                break;
            case "STATUS":
                try {
                    bug.setStatus(BugStatus.valueOf(newBugAttribute.toUpperCase()));
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException(String.format(VALIDATE_ENUM_ERROR_MESSAGE, bugTitle, state, newBugAttribute));
                }

                break;
            default:
                throw new IllegalArgumentException(String.format(INVALID_COMMAND, state));
        }

        String message = String.format(BUG_SET_MESSAGE_FORMAT, bugTitle, state, newBugAttribute);
        bug.addActivity(message);

        return message;
    }

    private void parseParameters(List<String> parameters) {
        try {
            bugTitle = parameters.get(0);
            state = parameters.get(1);
            newBugAttribute = parameters.get(2);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(CommandConstants.FAILED_TO_PARSE_PARAMETERS);
        }
    }
}
