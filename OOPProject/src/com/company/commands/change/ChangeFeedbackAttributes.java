package com.company.commands.change;

import com.company.commands.validatorHelper.CommandConstants;
import com.company.commands.validatorHelper.CommandValidator;
import com.company.core.contracts.Command;
import com.company.core.contracts.WIMRepository;
import com.company.models.contracts.Feedback;
import com.company.models.enums.FeedbackStatus;

import java.util.List;

public class ChangeFeedbackAttributes implements Command {

    private static final String INVALID_COMMAND = "Invalid command name: %s!";

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 3;
    private static final String FEEDBACK_SET_MESSAGE_FORMAT = "%s of feedback %s has been changed to %s.";
    private static final String VALIDATE_ENUM_ERROR_MESSAGE = "Failed to set %s enum of feedback %s to %s.";
    private static final String VALIDATE_RATING_ERROR_MESSAGE = "Failed to set rating of feedback %s to %s.";

    private final WIMRepository wimRepository;

    private String feedbackTitle;
    private String state;
    private String newFeedbackAttribute;

    public ChangeFeedbackAttributes(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkParametersSize(parameters.size(), CORRECT_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        CommandValidator.checkIfFeedbackExist(wimRepository, feedbackTitle);

        Feedback feedback = wimRepository.getFeedbacks().get(feedbackTitle);

        String newState = state.toUpperCase();

        switch (newState) {
            case "RATING":
                try {
                    feedback.setRating(Integer.parseInt(newFeedbackAttribute.trim()));
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException(String.format(VALIDATE_RATING_ERROR_MESSAGE, feedbackTitle, newFeedbackAttribute));
                }

                break;
            case "STATUS":
                try {
                    feedback.setStatus(FeedbackStatus.valueOf(newFeedbackAttribute.toUpperCase()));
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException(String.format(VALIDATE_ENUM_ERROR_MESSAGE, state, feedbackTitle, newFeedbackAttribute));
                }

                break;
            default:
                throw new IllegalArgumentException(String.format(INVALID_COMMAND, state));
        }

        String message = String.format(FEEDBACK_SET_MESSAGE_FORMAT, state, feedbackTitle, newFeedbackAttribute);
        feedback.addActivity(message);

        return message;
    }

    private void parseParameters(List<String> parameters) {
        try {
            feedbackTitle = parameters.get(0);
            state = parameters.get(1);
            newFeedbackAttribute = parameters.get(2);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(CommandConstants.FAILED_TO_PARSE_PARAMETERS);
        }
    }
}
